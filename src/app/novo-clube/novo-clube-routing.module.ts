import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NovoClubePage } from './novo-clube.page';

const routes: Routes = [
  {
    path: '',
    component: NovoClubePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NovoClubePageRoutingModule {}
