import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NovoClubePageRoutingModule } from './novo-clube-routing.module';

import { NovoClubePage } from './novo-clube.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NovoClubePageRoutingModule
  ],
  declarations: [NovoClubePage]
})
export class NovoClubePageModule {}
