import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../global.service';
import { Platform } from '@ionic/angular';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-novo-clube',
  templateUrl: './novo-clube.page.html',
  styleUrls: ['./novo-clube.page.scss'],
})
export class NovoClubePage implements OnInit {

  loading: any;
  type: any;
  club: any;

  responsible: any;
  name: any;
  cnpj: any;
  email: any;
  mobile_number: any;
  radio_px: any;
  cep: any;
  address: any;
  number: any;
  complement: any;
  state: any;
  city: any;
  associates: any;
  link: any;

  message: any;
  urlEp: any;

  qtdNotifications: any;

  headers = {
    'Content-Type': 'application/json',
    'Authorization': localStorage.getItem('token_type') + ' ' + localStorage.getItem('api_access_token')
  };

  constructor(
    private global: GlobalService,
    private platform: Platform,
    private http: HttpClient,
    private nativeHttp: HTTP,
    private loadingController: LoadingController,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  async showLoadingHandler() {
    if (this.loading == null) {
      this.loading = await this.loadingController.create({
        message: '',
      });
      this.loading.present();
    }
  } 

  public hideLoadingHandler() {
    if (this.loading != null) {
        this.loading.dismiss();
        this.loading = null;
    }
  }

  validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  onClickSubmit(formData) {
    if (formData.responsible == '' || formData.name == '' || formData.cnpj == '' || formData.email == '' || formData.mobile_number == '' || formData.radio_px == '' || formData.cep == '' || formData.address == '' || formData.number == '' || formData.complement == '' || formData.city == '' || formData.state == '' || formData.associates == '' || formData.link == '') 
    {
        this.global.presentAlert('Preencha os campos obrigatórios');
    }
    else if(formData.terms == undefined || formData.terms == false) { 
      this.global.presentAlert('É necessário marcar que leu e aceitou os nossos termos.');
    }
    else if(!this.validateEmail(formData.email)) {
      this.global.presentAlert('E-mail inválido!');
    }
    else
    {
      this.showLoadingHandler();

      let url = this.global.url + this.urlEp;

      let postData = {
        "user_id_intern": localStorage.getItem('user_id_intern'),
        "user_id": localStorage.getItem('user_id'),
        "responsible": formData.responsible,
        "name": formData.name,
        "cnpj": formData.cnpj,
        "email": formData.email,
        "mobile_number": formData.mobile_number,
        "radio_px": formData.radio_px,
        "cep": formData.cep,
        "address": formData.address,
        "number": formData.number,
        "complement": formData.complement,
        "city": formData.city,
        "state": formData.state,
        "associates": formData.associates,
        "link": formData.link,
      }

      if(this.platform.is('cordova'))
      {
        this.nativeHttp.post(url, postData, this.headers)
            .then(res => {
              this.hideLoadingHandler();
              this.global.presentAlert(this.message);
              this.router.navigate(['/clubes']);
            }, error => {
              alert('Erro ao cadastrar clube: ' + error);
              this.hideLoadingHandler();
            });
      }
      else
      {
        this.http.post(url, postData, { headers: this.headers})
          .subscribe(data => {
            this.hideLoadingHandler();
            this.global.presentAlert(this.message);
            this.router.navigate(['/clubes']);
            }, error => {
              alert('Erro ao cadastrar clube: ' + error);
              this.hideLoadingHandler();
        });
      }
    }
  }

  ngOnInit() {
    
    this.qtdNotifications = localStorage.getItem('qtdNotificationsBadge');
    setInterval(() => { 
       this.qtdNotifications = localStorage.getItem('qtdNotificationsBadge');
    }, 1000);
    
    this.route.paramMap.subscribe(params => {
      this.type = params.get("type");
    });

    if(this.type == 'edit') {
      this.message = 'Clube atualizado!';
      this.urlEp = '/api/updateClub';
      this.showLoadingHandler();
      let url = this.global.url + `/api/getClub/` + localStorage.getItem('user_id');

      if (this.platform.is('cordova')) {
        this.nativeHttp.get(url, {}, this.headers)
          .then(data => {

            this.club = JSON.parse(data['data']);

            this.responsible = this.club[0].responsible;
            this.name = this.club[0].name;
            this.cnpj = this.club[0].cnpj;
            this.email = this.club[0].email;
            this.mobile_number = this.club[0].mobile_number;
            this.radio_px = this.club[0].radio_px;
            this.cep = this.club[0].cep;
            this.address = this.club[0].address;
            this.number = this.club[0].number;
            this.complement = this.club[0].complement;
            this.state = this.club[0].state;
            this.city = this.club[0].city;
            this.associates = this.club[0].associates;
            this.link = this.club[0].link;
  
            this.hideLoadingHandler();
  
          }, error => {
            console.log(error);
          });
      }
      else {
        this.http.get(url, { headers: this.headers })
          .subscribe(data => {
  
            this.club = data[0];

            
            this.responsible = this.club.responsible;
            this.name = this.club.name;
            this.cnpj = this.club.cnpj;
            this.email = this.club.email;
            this.mobile_number = this.club.mobile_number;
            this.radio_px = this.club.radio_px;
            this.cep = this.club.cep;
            this.address = this.club.address;
            this.number = this.club.number;
            this.complement = this.club.complement;
            this.state = this.club.state;
            this.city = this.club.city;
            this.associates = this.club.associates;
            this.link = this.club.link;
  
            this.hideLoadingHandler();
  
          }, error => {
            console.log(error);
          });
      }
    }
    else {
      this.message = 'Clube cadastrado, aguarde liberação.';
      this.urlEp = '/api/newClub';
    }
  }

}
