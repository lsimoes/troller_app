import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NovoClubePage } from './novo-clube.page';

describe('NovoClubePage', () => {
  let component: NovoClubePage;
  let fixture: ComponentFixture<NovoClubePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NovoClubePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NovoClubePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
