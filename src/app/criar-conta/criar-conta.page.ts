import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../global.service';
import { Platform } from '@ionic/angular';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';

@Component({
  selector: 'app-criar-conta',
  templateUrl: './criar-conta.page.html',
  styleUrls: ['./criar-conta.page.scss'],
})
export class CriarContaPage implements OnInit {

  loading: any;
  typeVar: any;
  tokenVar: any;
  logged: any;
  continueForm = false;

  constructor(
    private global: GlobalService,
    private platform: Platform,
    private http: HttpClient,
    private nativeHttp: HTTP,
    private loadingController: LoadingController,
    private router: Router
  ) { }

  async ionViewWillEnter() {
    this.loading = await this.loadingController.create({
        message: '',
    });
  }

  public returnToken(type, token){
    localStorage.setItem('token_type', type);
    localStorage.setItem('access_token', token);
    this.typeVar = type;
    this.tokenVar = token;
  }

  ngOnInit() {
  }

  validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

 validateCpf(strCPF) {
    var Soma;
    var Resto;
    var i;
    Soma = 0;

    strCPF = strCPF.replace('.', '');
    strCPF = strCPF.replace('.', '');
    strCPF = strCPF.replace('-', '');
    // alert(strCPF)

  if (strCPF == "00000000000") return false;

  for (i=1; i<=9; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
  Resto = (Soma * 10) % 11;

    if ((Resto == 10) || (Resto == 11))  Resto = 0;
    if (Resto != parseInt(strCPF.substring(9, 10)) ) return false;

  Soma = 0;
    for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
    Resto = (Soma * 10) % 11;

    if ((Resto == 10) || (Resto == 11))  Resto = 0;
    if (Resto != parseInt(strCPF.substring(10, 11) ) ) return false;
    return true;
}

  public createAccount(name, cpf, email, email_confirmation, birthyear, password, password_confirmation) {
    let headers = {
      'Content-Type': 'application/json',
      'Authorization': this.typeVar + ' ' + this.tokenVar
    };

    let url = this.global.urlEp + '/api/contestants/register';

    let postData = {
        "name": name,
        "cpf": cpf,
        "email": email,
        "email_confirmation": email_confirmation,
        "birthyear": birthyear,
        "password": password,
        "password_confirmation": password_confirmation,
        "app_data": '123',
    }

    if (this.platform.is('cordova')) {
      this.nativeHttp.post(url, postData, headers)
        .then(res => {
          
          this.loading.dismiss();
          this.global.presentAlert('Obrigado por se cadastrar. Você já pode fazer o seu login.');
          this.router.navigate(['/login']);

        }, error => {
          alert('Erro ao criar conta: ' + JSON.stringify(error['error']));

          var retorno = error['error']
          var i;

          for (i=0; i<=retorno.length; i++)
          {
            console.log(retorno[i])
          }

              this.loading.dismiss();
        });
    }
    else {
      this.http.post(url, postData, { headers: headers })
        .subscribe(data => {
          
          this.loading.dismiss();
          this.global.presentAlert('Obrigado por se cadastrar. Você já pode fazer o seu login.');
          this.router.navigate(['/login']);

        }, error => {
              // alert('Erro ao criar conta: ' + JSON.stringify(error['error']['data']));

              var retorno = JSON.parse(JSON.stringify(error['error']['data']));
              var retornoItens = "";

              for (var key in retorno) {
               retornoItens += "- " + retorno[key][0] + "<br><br>";
              }

              this.global.presentAlert(retornoItens);

              this.loading.dismiss();
        });
    }
  }
  

  onClickSubmit(formData) {
    if (formData.name == '' || formData.cpf == '' || formData.email == '' || formData.email_confirmation == '' || formData.birthyear == '' || formData.password == '' || formData.password_confirmation == '') {
        this.global.presentAlert('Preencha todos os campos.');
        this.continueForm = false;
    }
    else if (!this.validateCpf(formData.cpf)) {
      this.global.presentAlert('CPF inválido.');
      this.continueForm = false;
    }
    else if (formData.email != formData.email_confirmation) {
      this.global.presentAlert('Confirmação de e-mail não corresponde.');
      this.continueForm = false;
    }
    else if (!this.validateEmail(formData.email)) {
      this.global.presentAlert('E-mail com formato incorreto.');
      this.continueForm = false;
    }
    else if (formData.birthyear.length < 4) {
      this.global.presentAlert('O ano de nascimento precisa ter 4 dígitos. Ex: 1986');
      this.continueForm = false;
    }
    else if (formData.birthyear < 1900) {
      this.global.presentAlert('Ano inválido.');
      this.continueForm = false;
    }
    else if (formData.password != formData.password_confirmation) {
      this.global.presentAlert('Confirmação de senha não corresponde.');
      this.continueForm = false;
    }
    else if (formData.password.length < 6) {
      this.global.presentAlert('A senha precisa ter no mínimo 6 caracteres.');
      this.continueForm = false;
    }
    else if(formData.terms == undefined || formData.terms == false) {
      this.global.presentAlert('Para utilizar o Mundo Troller é necessário marcar que leu e aceitou os nossos termos.');
      this.continueForm = false;
    }
    else {
      this.continueForm = true;
    }



    if(this.continueForm == true)
    {

        this.loading.present();

        ///Token
        // let postData1 = {
        //   "grant_type": 'client_credentials',
        //   "client_id": '1',
        //   "client_secret": 'f20V9MLF3Xjtvb0x9J3h8PodoLBGTk1qum9snCT1',
        //   "scope": 'events-integration',
        // }

        //produção
      let postData1 = {
        "grant_type": 'client_credentials',
        "client_id": '2',
        "client_secret": '5SRxqu7UG8qMhV7tmAsfX2PQTqxGvcqIxd3yk6Yr',
        "scope": 'events-integration',
      }

        let headers1 = {
          'Content-Type': 'application/json;'
        };

        let url1 = this.global.urlEp + '/api/oauth/token';

      

        if(this.platform.is('cordova'))
        {
          this.nativeHttp.post(url1, postData1, headers1)
            .then(data => {
              let info = JSON.parse(data['data']);
              this.returnToken(info['token_type'], info['access_token']);
              this.createAccount(formData.name, formData.cpf, formData.email, formData.email_confirmation, formData.birthyear, formData.password, formData.password_confirmation);
            })
            .catch(error => {
                alert('Erro ao gerar token. Tente novamente mais tarde.');
                this.loading.dismiss();
            });
        }
        else
        {
          return this.http.post(url1, postData1, { headers: headers1})
            .subscribe(data => {
                this.returnToken(data['token_type'], data['access_token']);
                this.createAccount(formData.name, formData.cpf, formData.email, formData.email_confirmation, formData.birthyear, formData.password, formData.password_confirmation);
              }, error => {
                alert('Erro ao gerar token. Tente novamente mais tarde.');
                this.loading.dismiss();
          });
        }
      
      }
    
  }


}
