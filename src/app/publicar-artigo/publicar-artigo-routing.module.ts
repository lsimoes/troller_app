import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PublicarArtigoPage } from './publicar-artigo.page';

const routes: Routes = [
  {
    path: '',
    component: PublicarArtigoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PublicarArtigoPageRoutingModule {}
