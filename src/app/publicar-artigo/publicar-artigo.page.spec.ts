import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PublicarArtigoPage } from './publicar-artigo.page';

describe('PublicarArtigoPage', () => {
  let component: PublicarArtigoPage;
  let fixture: ComponentFixture<PublicarArtigoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicarArtigoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PublicarArtigoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
