import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PublicarArtigoPageRoutingModule } from './publicar-artigo-routing.module';

import { PublicarArtigoPage } from './publicar-artigo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PublicarArtigoPageRoutingModule
  ],
  declarations: [PublicarArtigoPage]
})
export class PublicarArtigoPageModule {}
