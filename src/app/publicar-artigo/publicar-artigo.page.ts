import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../global.service';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';
import {NgForm} from '@angular/forms';
import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';

@Component({
  selector: 'app-publicar-artigo',
  templateUrl: './publicar-artigo.page.html',
  styleUrls: ['./publicar-artigo.page.scss'],
})
export class PublicarArtigoPage implements OnInit {

  loading: any;
  statusDealer: any;
  statusClub: any;
  qtdNotifications: any;

  headers = {
    'Content-Type': 'application/json',
    'Authorization': localStorage.getItem('token_type') + ' ' + localStorage.getItem('api_access_token')
  };

  constructor(
    private global: GlobalService,
    private http: HttpClient,
    private loadingController: LoadingController,
    private router: Router,
    private platform: Platform,
    private nativeHttp: HTTP
  ) { }

  async showLoadingHandler() {
    if (this.loading == null) {
      this.loading = await this.loadingController.create({
        message: '',
      });
      this.loading.present();
    }
  } 

  public hideLoadingHandler() {
    if (this.loading != null) {
        this.loading.dismiss();
        this.loading = null;
    }
  }

  ngOnInit() {

    this.qtdNotifications = localStorage.getItem('qtdNotificationsBadge');
    setInterval(() => { 
       this.qtdNotifications = localStorage.getItem('qtdNotificationsBadge');
    }, 1000);

    let url = this.global.url + `/api/checkDealer/` + localStorage.getItem('user_id');
    let url2 = this.global.url + `/api/checkClub/` + localStorage.getItem('user_id');

    if (this.platform.is('cordova')) {
      this.nativeHttp.get(url, {}, this.headers)
        .then(data => {
          this.statusDealer = JSON.parse(data['data']);
        }, error => {
          console.log(error);
        });

      this.nativeHttp.get(url2, {}, this.headers)
      .then(data => {
        this.statusClub = JSON.parse(data['data']);
      }, error => {
        console.log(error);
      });
    }
    else {
      this.http.get(url, { headers: this.headers })
      .subscribe(data => {
        this.statusDealer = data;
      }, error => {
        console.log(error);
      });

      this.http.get(url2, { headers: this.headers })
      .subscribe(data => {
        this.statusClub = data;
      }, error => {
        console.log(error);
      });
    }
  }

  onClickSubmit(f: NgForm, formData) {

    if(formData.title == undefined || formData.text == undefined) // || this.filedata == undefined
    {
      alert("Preencha todos os campos!");
    }
    else
    {
      this.showLoadingHandler();

      let postData = {
        "user_id": localStorage.getItem('user_id'),
        "type": 'article',
        "title": formData.title,
        "text": formData.text,
        "session": formData.session
      }

      let url = this.global.url+"/api/newPublication";

      if(this.platform.is('cordova'))
      {
        this.nativeHttp.post(url, postData, this.headers)
            .then(res => {
              alert("Artigo cadastrado!");
              this.hideLoadingHandler();
              this.router.navigate(['home']);
            }, error => {
              alert(JSON.stringify(error));
            });
      }
      else {
        this.http.post(url, postData, { headers: this.headers
        }).subscribe(data => {
            alert("Artigo cadastrado!");
            this.hideLoadingHandler();
            this.router.navigate(['home']);
          }, error => {
            console.log(error);
        });
      }

    }
  }

}
