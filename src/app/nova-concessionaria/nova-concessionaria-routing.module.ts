import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NovaConcessionariaPage } from './nova-concessionaria.page';

const routes: Routes = [
  {
    path: '',
    component: NovaConcessionariaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NovaConcessionariaPageRoutingModule {}
