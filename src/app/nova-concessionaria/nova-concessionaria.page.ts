import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../global.service';
import { Platform } from '@ionic/angular';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
import { ActivatedRoute } from "@angular/router";


@Component({
  selector: 'app-nova-concessionaria',
  templateUrl: './nova-concessionaria.page.html',
  styleUrls: ['./nova-concessionaria.page.scss'],
})
export class NovaConcessionariaPage implements OnInit {

  loading: any;
  type: any;
  dealer: any;

  name: any;
  cnpj: any;
  email: any;
  phone_number: any;
  cep: any;
  address: any;
  number: any;
  complement: any;
  state: any;
  city: any;
  opening_hours: any;
  link: any;

  message: any;
  urlEp: any;

  qtdNotifications: any;

  headers = {
    'Content-Type': 'application/json',
    'Authorization': localStorage.getItem('token_type') + ' ' + localStorage.getItem('api_access_token')
  };

  constructor(
    private global: GlobalService,
    private platform: Platform,
    private http: HttpClient,
    private nativeHttp: HTTP,
    private loadingController: LoadingController,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  async showLoadingHandler() {
    if (this.loading == null) {
      this.loading = await this.loadingController.create({
        message: '',
      });
      this.loading.present();
    }
  } 

  public hideLoadingHandler() {
    if (this.loading != null) {
        this.loading.dismiss();
        this.loading = null;
    }
  }

  validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  onClickSubmit(formData) {
    if (formData.name == '' || formData.cnpj == '' || formData.email == '' || formData.phone_number == '' || formData.cep == '' || formData.address == '' || formData.number == '' || formData.complement == '' || formData.city == '' || formData.state == '' || formData.opening_hours == '' || formData.link == '') 
    {
        this.global.presentAlert('Preencha os campos obrigatórios');
    }
    else if(formData.terms == undefined || formData.terms == false) { 
      this.global.presentAlert('É necessário marcar que leu e aceitou os nossos termos.');
    }
    else if(!this.validateEmail(formData.email)) {
      this.global.presentAlert('E-mail inválido!');
    }
    else
    {
      this.showLoadingHandler();

      let url = this.global.url + this.urlEp;

      let postData = {
        "user_id": localStorage.getItem('user_id'),
        "name": formData.name,
        "cnpj": formData.cnpj,
        "email": formData.email,
        "phone_number": formData.phone_number,
        "cep": formData.cep,
        "address": formData.address,
        "number": formData.number,
        "complement": formData.complement,
        "city": formData.city,
        "state": formData.state,
        "opening_hours": formData.opening_hours,
        "link": formData.link,
      }

      if(this.platform.is('cordova'))
      {
        this.nativeHttp.post(url, postData, this.headers)
            .then(res => {
              this.hideLoadingHandler();
              this.global.presentAlert(this.message);
              this.router.navigate(['/concessionarias']);
            }, error => {
              alert('Erro ao atualizar concessionária: ' + error);
              this.hideLoadingHandler();
            });
      }
      else
      {
        this.http.post(url, postData, { headers: this.headers})
          .subscribe(data => {
            this.hideLoadingHandler();
            this.global.presentAlert(this.message);
            this.router.navigate(['/concessionarias']);
            }, error => {
              alert('Erro ao atualizar concessionária: ' + error);
              this.hideLoadingHandler();
        });
      }
    }
  }

  ngOnInit() {
    this.showLoadingHandler();

    this.qtdNotifications = localStorage.getItem('qtdNotificationsBadge');
    setInterval(() => { 
       this.qtdNotifications = localStorage.getItem('qtdNotificationsBadge');
    }, 1000);
    
    this.route.paramMap.subscribe(params => {
      this.type = params.get("type");
    });

    if(this.type == 'edit') {
      this.message = 'Concessionária atualizada!';
      this.urlEp = '/api/updateDealer';

      let url = this.global.url + `/api/getDealer/` + localStorage.getItem('user_id');

      if (this.platform.is('cordova')) {
        this.nativeHttp.get(url, {}, this.headers)
          .then(data => {

            this.dealer = JSON.parse(data['data']);

            this.name = this.dealer[0].name;
            this.cnpj = this.dealer[0].cnpj;
            this.email = this.dealer[0].email;
            this.phone_number = this.dealer[0].phone_number;
            this.cep = this.dealer[0].cep;
            this.address = this.dealer[0].address;
            this.number = this.dealer[0].number;
            this.complement = this.dealer[0].complement;
            this.state = this.dealer[0].state;
            this.city = this.dealer[0].city;
            this.opening_hours = this.dealer[0].opening_hours;
            this.link = this.dealer[0].link;
  
            this.hideLoadingHandler();
  
          }, error => {
            console.log(error);
          });
      }
      else {
        this.http.get(url, { headers: this.headers })
          .subscribe(data => {

            this.dealer = data[0];

            this.name = this.dealer.name;
            this.cnpj = this.dealer.cnpj;
            this.email = this.dealer.email;
            this.phone_number = this.dealer.phone_number;
            this.cep = this.dealer.cep;
            this.address = this.dealer.address;
            this.number = this.dealer.number;
            this.complement = this.dealer.complement;
            this.state = this.dealer.state;
            this.city = this.dealer.city;
            this.opening_hours = this.dealer.opening_hours;
            this.link = this.dealer.link;
  
            this.hideLoadingHandler();
  
          }, error => {
            console.log(error);
          });
      }
    }
    else {
      this.message = '';
      this.urlEp = '';
    }
  }

}
