import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NovaConcessionariaPageRoutingModule } from './nova-concessionaria-routing.module';

import { NovaConcessionariaPage } from './nova-concessionaria.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NovaConcessionariaPageRoutingModule
  ],
  declarations: [NovaConcessionariaPage]
})
export class NovaConcessionariaPageModule {}
