import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NovaConcessionariaPage } from './nova-concessionaria.page';

describe('NovaConcessionariaPage', () => {
  let component: NovaConcessionariaPage;
  let fixture: ComponentFixture<NovaConcessionariaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NovaConcessionariaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NovaConcessionariaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
