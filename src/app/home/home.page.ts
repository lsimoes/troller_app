import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../global.service';
import { ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { Platform } from '@ionic/angular';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Router } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {


	@ViewChild('slides', {static: true}) slides: IonSlides;


	banners: any;
  qtdNotifications: any;

  constructor(
    private global: GlobalService,
    private platform: Platform,
    private http: HttpClient,
    private nativeHttp: HTTP,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {

    this.qtdNotifications = localStorage.getItem('qtdNotificationsBadge');
    setInterval(() => { 
       this.qtdNotifications = localStorage.getItem('qtdNotificationsBadge');
    }, 1000);

  	let headers = {
      'Content-Type': 'application/json',
      'Authorization': localStorage.getItem('token_type') + ' ' + localStorage.getItem('api_access_token')
    };

    let url = this.global.url + `/api/getBanners`;

    if (this.platform.is('cordova')) {
      this.nativeHttp.get(url, {}, headers)
        .then(res => {

          let data = JSON.parse(res['data']);

          // this.loading = false;
          
          this.banners = data;

        }, error => {
          console.log(error);
        });
    }
    else {
      this.http.get(url, { headers: headers })
        .subscribe(data => {
          // this.loading = false;
          
          this.banners = data;

          console.log(data);
         }, error => {
          console.log(error);
      });
    }


  }

}
