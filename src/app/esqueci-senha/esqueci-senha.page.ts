import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
import { GlobalService } from '../global.service';

@Component({
  selector: 'app-esqueci-senha',
  templateUrl: './esqueci-senha.page.html',
  styleUrls: ['./esqueci-senha.page.scss'],
})
export class EsqueciSenhaPage implements OnInit {

  loading: any;
  typeVar: any;
  tokenVar: any;

  constructor(
    private global: GlobalService,
    private platform: Platform,
    private http: HttpClient,
    private nativeHttp: HTTP,
    private loadingController: LoadingController,
    private router: Router) { }

  async ionViewWillEnter() {
      this.loading = await this.loadingController.create({
        message: '',
      });
    }

  ngOnInit() {
  }

  public returnToken(type, token){
    localStorage.setItem('token_type', type);
    localStorage.setItem('access_token', token);
    this.typeVar = type;
    this.tokenVar = token;
  }

  public recover(cpf, email) {


    let postData = {
      "cpf": cpf,
      "email": email
    }

    let headers = {
      'Content-Type': 'application/json',
      'Authorization': this.typeVar + ' ' + this.tokenVar
    };

    let url = this.global.urlEp + '/api/contestants/forgotPassword';

    if (this.platform.is('cordova')) {
      this.nativeHttp.post(url, postData, headers)
        .then(res => {

          this.global.presentAlert('Obrigado. Você irá receber um link de recuperação de senha no seu e-mail.');
          this.loading.dismiss();
          this.router.navigate(['/login']);

          
        }, error => {
          alert('Usuário não encontrado.');
          // alert('Erro ao recuperar senha: ' + JSON.stringify(error['error']));
              this.loading.dismiss();
        });
    }
    else {
      this.http.post(url, postData, { headers: headers })
        .subscribe(data => {
            this.global.presentAlert('Obrigado. Você irá receber um link de recuperação de senha no seu e-mail.');
            this.loading.dismiss();
            this.router.navigate(['/login']);
        }, error => {
          alert('Usuário não encontrado.');
          // alert('Erro ao recuperar senha: ' + JSON.stringify(error['error']));
              this.loading.dismiss();
        });
    }
  }

  onClickSubmit(formData) {
    

    if (formData.cpf == '' || formData.password == '') {
      alert("Preencha todos os campos");
    }
    else {
      this.loading.present();

      ///Token
      let postData1 = {
        "grant_type": 'client_credentials',
        "client_id": '1',
        "client_secret": 'f20V9MLF3Xjtvb0x9J3h8PodoLBGTk1qum9snCT1',
        "scope": 'events-integration',
      }

      let headers1 = {
        'Content-Type': 'application/json'
      };

      let url1 = this.global.urlEp + '/api/oauth/token';

    

      if(this.platform.is('cordova'))
      {
        this.nativeHttp.post(url1, postData1, headers1)
            .then(res => {
                let data = JSON.parse(res['data']);
                this.returnToken(data['token_type'], data['access_token']);
                this.recover(formData.cpf, formData.email);
              }, error => {
          this.global.presentAlert('Erro interno.');
          this.loading.dismiss();
        });
      }
      else
      {
        this.http.post(url1, postData1, { headers: headers1})
          .subscribe(data => {
              this.returnToken(data['token_type'], data['access_token']);
              this.recover(formData.cpf, formData.email);
            }, error => {
        this.global.presentAlert('Erro interno.');
        this.loading.dismiss();
        });
      }
    }
  }

}
