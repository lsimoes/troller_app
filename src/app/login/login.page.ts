import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
import { GlobalService } from '../global.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loading: any;
  typeVar: any;
  tokenVar: any;
  logged: any;

  constructor(
    private global: GlobalService,
    private platform: Platform,
    private http: HttpClient,
    private nativeHttp: HTTP,
    private loadingController: LoadingController,
    private router: Router) { }

  async ionViewWillEnter() {
    this.loading = await this.loadingController.create({
      message: '',
    });

    // if (localStorage.getItem('logged') != 'true') {
    //   this.router.navigate(['/']);
    // }
    // else {
    //   this.router.navigate(['home']);
    // }
  }

  public returnToken(type, token){
    localStorage.setItem('token_type', type);
    localStorage.setItem('access_token', token);
    this.typeVar = type;
    this.tokenVar = token;
  }

  ngOnInit() {
    if(!localStorage.getItem('logged') || localStorage.getItem('logged') != 'true')
      {
        this.logged = 'false';
      }
      else
      {
        this.logged = 'true';
        this.router.navigate(['/home']);
        // this.userId = localStorage.getItem('localUserId');
      }
  }

  public generateApiToken(email, password) {
      let postData = {
        "grant_type": 'password',
        "client_id": 3,
        "client_secret": 'yKNVyqijfLPgJ2Bk5uSAm9NfRChPqHPQvsLIMQjU',
        "username": email,
        "password": password,
        "scope": ''
      }

      let headers = {
        'Content-Type': 'application/json',
        // 'Authorization': this.typeVar + ' ' + this.tokenVar
      };

      let url = this.global.url + `/oauth/token`;

      if (this.platform.is('cordova')) {
        this.nativeHttp.post(url, postData, headers)
          .then(res => {
  
            let data = JSON.parse(res['data']);
  
            localStorage.setItem('api_access_token', data['access_token']);
            this.loading.dismiss();
            this.router.navigate(['home']);
            
          }, error => {
            alert('Erro no login: ' + JSON.stringify(error['error']));
                this.loading.dismiss();
          });
      }
      else {
        this.http.post(url, postData, { headers: headers })
          .subscribe(data => {
            localStorage.setItem('api_access_token', data['access_token']);
            this.loading.dismiss();
            this.router.navigate(['home']);
          }, error => {
            alert('Erro no login: ' + JSON.stringify(error['error']));
                this.loading.dismiss();
          });
      }

  }

  public returnLogin(user_id, name, nickname, birthdate, cpf, cep, address, number, complement, city_id , neighborhood, mobile_number, email, password) {
    localStorage.setItem('logged', 'true');
    localStorage.setItem('user_id', user_id);
    localStorage.setItem('name', name);
    localStorage.setItem('nickname', nickname);
    localStorage.setItem('birthdate', birthdate);
    localStorage.setItem('cpf', cpf);
    localStorage.setItem('cep', cep);
    localStorage.setItem('address', address);
    localStorage.setItem('number', number);
    localStorage.setItem('complement', complement);
    localStorage.setItem('neighborhood', neighborhood);
    localStorage.setItem('city_id', city_id);
    localStorage.setItem('mobile_number', mobile_number);
    localStorage.setItem('email', email);

    let postData = {
      "user_id": user_id,
      "name": name,
      "email": email,
      "cpf": cpf,
      "password": password
    }

    

    let headers = {
      'Content-Type': 'application/json',
      // 'Authorization': this.typeVar + ' ' + this.tokenVar
    };

    
    let url = this.global.url + `/api/login-api`;

    if (this.platform.is('cordova')) {
      this.nativeHttp.post(url, postData, headers)
        .then(res => {

          this.generateApiToken(email, password);
          // localStorage.setItem('user_id_intern', 'true');
          
          let data = JSON.parse(res['data']);

          // alert(data['id'])

          localStorage.setItem('user_id_intern', data['id']);
          
        }, error => {
          alert('Erro no login: ' + JSON.stringify(error['error']));
              this.loading.dismiss();
        });
    }
    else {
      this.http.post(url, postData, { headers: headers })
        .subscribe(data => {
          this.generateApiToken(email, password);
          localStorage.setItem('user_id_intern', data['id']);
        }, error => {
          alert('Erro no login: ' + JSON.stringify(error['error']));
              this.loading.dismiss();
        });
    }
  }

  public login(cpf, password) {


    let postData = {
      "cpf": cpf,
      "password": password
    }

    let headers = {
      'Content-Type': 'application/json',
      'Authorization': this.typeVar + ' ' + this.tokenVar
    };

    let url = this.global.urlEp + '/api/contestants/validate-login';

    if (this.platform.is('cordova')) {
      this.nativeHttp.post(url, postData, headers)
        .then(res => {

          let data = JSON.parse(res['data']);

          this.returnLogin(data['data']["id"], data['data']["name"], data['data']["nickname"], data['data']["birthdate"], data['data']["cpf"], data['data']["cep"], data['data']["address"], data['data']["number"], data['data']["complement"], data['data']["city_id"] ,data['data']["neighborhood"], data['data']["mobile_number"], data['data']['main_email'][0]['email'], postData['password']);
        }, error => {
          this.global.presentAlert(JSON.stringify(error['error']['error']));
              this.loading.dismiss();
        });
    }
    else {
      this.http.post(url, postData, { headers: headers })
        .subscribe(data => {
          this.returnLogin(data['data']["id"], data['data']["name"], data['data']["nickname"], data['data']["birthdate"], data['data']["cpf"], data['data']["cep"], data['data']["address"], data['data']["number"], data['data']["complement"], data['data']["city_id"], data['data']["neighborhood"], data['data']["mobile_number"], data['data']['main_email'][0]['email'], postData['password']);
        }, error => {
          this.global.presentAlert(JSON.stringify(error['error']['error']));
              this.loading.dismiss();
        });
    }

  }

  onClickSubmit(formData) {
    

    if (formData.cpf == '' || formData.password == '') {
      alert("Preencha todos os campos");
    }
    else {
      this.loading.present();

      ///Token 
      //homologação
      // let postData1 = {
      //   "grant_type": 'client_credentials',
      //   "client_id": '1',
      //   "client_secret": 'f20V9MLF3Xjtvb0x9J3h8PodoLBGTk1qum9snCT1',
      //   "scope": 'events-integration',
      // }

      //produção
      let postData1 = {
        "grant_type": 'client_credentials',
        "client_id": '2',
        "client_secret": '5SRxqu7UG8qMhV7tmAsfX2PQTqxGvcqIxd3yk6Yr',
        "scope": 'events-integration',
      }

      let headers1 = {
        'Content-Type': 'application/json'
      };

      let url1 = this.global.urlEp + '/api/oauth/token';

    

      if(this.platform.is('cordova'))
      {
        this.nativeHttp.post(url1, postData1, headers1)
            .then(res => {
                let data = JSON.parse(res['data']);
                this.returnToken(data['token_type'], data['access_token']);
                this.login(formData.cpf, formData.password);
              }, error => {
          alert(error);
        });
      }
      else
      {
        this.http.post(url1, postData1, { headers: headers1})
          .subscribe(data => {
              this.returnToken(data['token_type'], data['access_token']);
              this.login(formData.cpf, formData.password);
            }, error => {
        console.log(error);
        });
      }
    }
  }
}
