import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EventoSobrePage } from './evento-sobre.page';

describe('EventoSobrePage', () => {
  let component: EventoSobrePage;
  let fixture: ComponentFixture<EventoSobrePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventoSobrePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EventoSobrePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
