import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EventoSobrePage } from './evento-sobre.page';

const routes: Routes = [
  {
    path: '',
    component: EventoSobrePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EventoSobrePageRoutingModule {}
