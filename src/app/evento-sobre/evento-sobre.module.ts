import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EventoSobrePageRoutingModule } from './evento-sobre-routing.module';

import { EventoSobrePage } from './evento-sobre.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EventoSobrePageRoutingModule
  ],
  declarations: [EventoSobrePage]
})
export class EventoSobrePageModule {}
