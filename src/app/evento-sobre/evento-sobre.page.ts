import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../global.service';
import { Platform } from '@ionic/angular';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-evento-sobre',
  templateUrl: './evento-sobre.page.html',
  styleUrls: ['./evento-sobre.page.scss'],
})
export class EventoSobrePage implements OnInit {

  id: any;
  name: any;
  title: any;
  highlight_description: any;
  thumb: any;
  eventId: any;
  loading: any;
  qtdNotifications: any;

  constructor(
    private global: GlobalService,
    private platform: Platform,
    private http: HttpClient,
    private nativeHttp: HTTP,
    private loadingController: LoadingController,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  async showLoadingHandler() {
    if (this.loading == null) {
      this.loading = await this.loadingController.create({
        message: '',
      });
      this.loading.present();
    }
  } 

  public hideLoadingHandler() {
    if (this.loading != null) {
        this.loading.dismiss();
        this.loading = null;
    }
  }

  ngOnInit() {
    this.showLoadingHandler();

    this.qtdNotifications = localStorage.getItem('qtdNotificationsBadge');
    setInterval(() => { 
       this.qtdNotifications = localStorage.getItem('qtdNotificationsBadge');
    }, 1000);

    this.route.paramMap.subscribe(params => {
      this.id = params.get("id");
    })

    let headers = {
      'Content-Type': 'application/json',
      'Authorization': localStorage.getItem('token_type') + ' ' + localStorage.getItem('access_token')
    };

    let url = this.global.urlEp + '/api/events/details/' + this.id;

    if (this.platform.is('cordova')) {
      this.nativeHttp.get(url, {}, headers)
        .then(res2 => {


          let data = JSON.parse(res2['data']);

          this.name = data['data']['name'];
          this.title = data['data']['title'];
          this.highlight_description = data['data']['highlight_description'];
          this.thumb = data['data']['highlight_img_url'];
          this.eventId = data['data']['id'];

          this.hideLoadingHandler();

        }, error => {
          console.log(error);
        });
    }
    else {
      this.http.get(url, { headers: headers })
        .subscribe(data2 => {

          this.name = data2['data']['name'];
          this.title = data2['data']['title'];
          this.highlight_description = data2['data']['highlight_description'];
          this.thumb = data2['data']['highlight_img_url'];
          this.eventId = data2['data']['id'];

          this.hideLoadingHandler();

        }, error => {
          console.log(error);
        });
    }
  }

}
