import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { GlobalService } from '../global.service';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { ActionSheetController, Platform } from '@ionic/angular';
import {
  MediaCapture,
  MediaFile,
  CaptureError
} from '@ionic-native/media-capture/ngx';
import { File, FileEntry } from '@ionic-native/File/ngx';
import { Media, MediaObject } from '@ionic-native/media/ngx';
import { StreamingMedia } from '@ionic-native/streaming-media/ngx';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';

import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';

import { HTTP } from '@ionic-native/http/ngx';

const MEDIA_FOLDER_NAME = 'my_media';

@Component({
  selector: 'app-publicar-audio',
  templateUrl: './publicar-audio.page.html',
  styleUrls: ['./publicar-audio.page.scss'],
})
export class PublicarAudioPage implements OnInit {

  files = [];
  medias = [];
  datenow: any;
  filedata:any;
  filedata0: any;
  filedata0name: any;
  loading: any;
  extFile: any;

  statusDealer: any;
  statusClub: any;
  qtdNotifications: any;

  headers = {
    'Content-Type': 'application/json',
    'Authorization': localStorage.getItem('token_type') + ' ' + localStorage.getItem('api_access_token')
  };

  constructor(
    private router: Router,
    private global: GlobalService,
    private imagePicker: ImagePicker,
    private mediaCapture: MediaCapture,
    private file: File,
    private media: Media,
    private photoViewer: PhotoViewer,
    private plt: Platform,
    private transfer: FileTransfer,
    private loadingController: LoadingController,
    private streamingMedia: StreamingMedia,
    private actionSheetController: ActionSheetController,
    private http: HttpClient,
    private platform: Platform,
    private nativeHttp: HTTP,
  ) { }

  async showLoadingHandler() {
    if (this.loading == null) {
      this.loading = await this.loadingController.create({
        message: 'Aguarde. Estamos processando este arquivo.',
      });
      this.loading.present();
    }
  } 

  public hideLoadingHandler() {
    if (this.loading != null) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  ngOnInit() {
    this.qtdNotifications = localStorage.getItem('qtdNotificationsBadge');
    setInterval(() => { 
       this.qtdNotifications = localStorage.getItem('qtdNotificationsBadge');
    }, 1000);

    this.datenow = Date.now();

    this.plt.ready().then(() => {
      let path = this.file.dataDirectory;
      this.file.checkDir(path, MEDIA_FOLDER_NAME + this.datenow).then(
        () => {
          this.loadFiles();
        },
        err => {
          this.file.createDir(path, MEDIA_FOLDER_NAME + this.datenow, false);
        }
      );
    });


    let url = this.global.url + `/api/checkDealer/` + localStorage.getItem('user_id');
    let url2 = this.global.url + `/api/checkClub/` + localStorage.getItem('user_id');

    if (this.platform.is('cordova')) {
      this.nativeHttp.get(url, {}, this.headers)
        .then(data => {
          this.statusDealer = JSON.parse(data['data']);
        }, error => {
          console.log(error);
        });

      this.nativeHttp.get(url2, {}, this.headers)
      .then(data => {
        this.statusClub = JSON.parse(data['data']);
      }, error => {
        console.log(error);
      });
    }
    else {
      this.http.get(url, { headers: this.headers })
      .subscribe(data => {
        this.statusDealer = data;
      }, error => {
        console.log(error);
      });

      this.http.get(url2, { headers: this.headers })
      .subscribe(data => {
        this.statusClub = data;
      }, error => {
        console.log(error);
      });
    }
  }

  loadFiles() {
    this.file.listDir(this.file.dataDirectory, MEDIA_FOLDER_NAME + this.datenow).then(
      res => {
        this.files = res;
      },
      err => console.log('error loading files: ', err)
    );
  }

  recordAudio() {
    this.mediaCapture.captureAudio().then(
      (data: MediaFile[]) => {
        if (data.length > 0) {
          this.copyFileToLocalDir(data[0].fullPath);
        }
      },
      (err: CaptureError) => console.error(err)
    );
  }

  copyFileToLocalDir(fullPath) {

    this.showLoadingHandler();

    let myPath = fullPath;
    // Make sure we copy from the right location
    if (fullPath.indexOf('file://') < 0) {
      myPath = 'file://' + fullPath;
    }

    const ext = myPath.split('.').pop();
    const d = Date.now();
    const newName = `${d}.${ext}`;

    this.extFile = ext;

    const name = myPath.substr(myPath.lastIndexOf('/') + 1);
    const copyFrom = myPath.substr(0, myPath.lastIndexOf('/') + 1);
    const copyTo = this.file.dataDirectory + MEDIA_FOLDER_NAME;

    this.filedata0 = copyTo;
    this.filedata0name = newName;

    this.medias.push({ name: this.filedata0name, uri: myPath});

    let fileTransfer: FileTransferObject = this.transfer.create();

    let options: FileUploadOptions = {
      fileKey: 'media0',
      fileName: this.filedata0name,
      mimeType: 'multipart/form-data',
      headers: {}
    }
    
    fileTransfer.upload(myPath, this.global.url+'/api/upload-file', options)
    .then((data) => {
      this.hideLoadingHandler();
      // this.loadFiles();
      this.files = this.medias;
      // alert(JSON.stringify(data));
    }, (err) => {
      alert('erro no upload');
      alert(JSON.stringify(err));
    })

    // this.file.copyFile(copyFrom, name, copyTo, newName).then(
    //   success => {

    //     this.medias.push({ name: this.filedata0name });

    //     let fileTransfer: FileTransferObject = this.transfer.create();

    //     let options: FileUploadOptions = {
    //       fileKey: 'media0',
    //       fileName: this.filedata0name,
    //       mimeType: 'multipart/form-data',
    //       headers: {}
    //     }
    //     // alert(this.filedata0 + '/' + this.filedata0name);
    //     fileTransfer.upload(myPath, this.global.url + '/api/upload-file', options)
    //       .then((data) => {
    //         this.hideLoadingHandler();
    //         // alert(JSON.stringify(data));
    //       }, (err) => {
    //         // alert('erro no upload');
    //         alert(JSON.stringify(err));
    //       })


    //     this.loadFiles();
    //   },
    //   error => {
    //     console.log('error: ', error);
    //   }
    // );
  }

  openFile(f: FileEntry) {
    if (f.name.indexOf('.wav') > -1) {
      // We need to remove file:/// from the path for the audio plugin to work
      const path = f.nativeURL.replace(/^file:\/\//, '');
      const audioFile: MediaObject = this.media.create(path);
      audioFile.play();
    } else if (f.name.indexOf('.MOV') > -1 || f.name.indexOf('.mp4') > -1) {
      // E.g: Use the Streaming Media plugin to play a video
      this.streamingMedia.playVideo(f.nativeURL);
    } else if (f.name.indexOf('.jpg') > -1) {
      // E.g: Use the Photoviewer to present an Image
      this.photoViewer.show(f.nativeURL, 'Foto');
    }
  }

  deleteFile(f: FileEntry) {
    const path = f.nativeURL.substr(0, f.nativeURL.lastIndexOf('/') + 1);
    this.file.removeFile(path, f.name).then(() => {

      var fileObj = this.medias.find(x => x.name == f.name);

      this.medias.splice(this.medias.indexOf(fileObj), 1)

      this.loadFiles();
    }, err => console.log('error remove: ', err));
  }

  onClickSubmit(f: NgForm, formData) {
    
    if(formData.title == undefined || this.medias.length == 0) 
    {
      alert("Preencha todos os campos!");
    }
    else
    {
      this.showLoadingHandler();

      var files = '';

      for(var i=0; i< this.medias.length; i++)
      {
        files += this.medias[i]['name'];
      }

      let url = this.global.url+"/api/newPublication";

      let postData = {
        "user_id": localStorage.getItem('user_id'),
        "type": 'audio',
        "title": formData.title,
        "session": formData.session,
        "file": files,
      }

      if(this.platform.is('cordova'))
      {
        this.nativeHttp.post(url, postData, this.headers)
            .then(res => {
              alert("Audio cadastrado!");
              this.router.navigate(['home']);
              this.hideLoadingHandler();
            }, error => {
              alert(JSON.stringify(error));
            });
      }
      else {
        this.http.post(url, postData, { headers: this.headers
        }).subscribe(data => {
          alert("Audio cadastrado!");
          this.router.navigate(['home']);
          this.hideLoadingHandler();
        }, error => {
          console.log(error);
        });
      }

      
    }
  }

}
