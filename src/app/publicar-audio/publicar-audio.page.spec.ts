import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PublicarAudioPage } from './publicar-audio.page';

describe('PublicarAudioPage', () => {
  let component: PublicarAudioPage;
  let fixture: ComponentFixture<PublicarAudioPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicarAudioPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PublicarAudioPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
