import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PublicarAudioPageRoutingModule } from './publicar-audio-routing.module';

import { PublicarAudioPage } from './publicar-audio.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PublicarAudioPageRoutingModule
  ],
  declarations: [PublicarAudioPage]
})
export class PublicarAudioPageModule {}
