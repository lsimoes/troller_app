import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PublicarAudioPage } from './publicar-audio.page';

const routes: Routes = [
  {
    path: '',
    component: PublicarAudioPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PublicarAudioPageRoutingModule {}
