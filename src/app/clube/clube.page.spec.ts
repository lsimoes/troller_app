import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ClubePage } from './clube.page';

describe('ClubePage', () => {
  let component: ClubePage;
  let fixture: ComponentFixture<ClubePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClubePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ClubePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
