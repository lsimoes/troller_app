import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../global.service';
import { Platform } from '@ionic/angular';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-clube',
  templateUrl: './clube.page.html',
  styleUrls: ['./clube.page.scss'],
})
export class ClubePage implements OnInit {

  id:any;
  club: any;
  loading: any;
  qtdNotifications: any;

  constructor(
    private global: GlobalService,
    private platform: Platform,
    private http: HttpClient,
    private nativeHttp: HTTP,
    private loadingController: LoadingController,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  async showLoadingHandler() {
    if (this.loading == null) {
      this.loading = await this.loadingController.create({
        message: '',
      });
      this.loading.present();
    }
  } 

  public hideLoadingHandler() {
    if (this.loading != null) {
        this.loading.dismiss();
        this.loading = null;
    }
  }

  ngOnInit() {
    this.showLoadingHandler();

    this.qtdNotifications = localStorage.getItem('qtdNotificationsBadge');
    setInterval(() => { 
       this.qtdNotifications = localStorage.getItem('qtdNotificationsBadge');
    }, 1000);

    this.route.paramMap.subscribe(params => {
      this.id = params.get("id");
    })

    let headers = {
      'Content-Type': 'application/json',
      'Authorization': localStorage.getItem('token_type') + ' ' + localStorage.getItem('api_access_token')
    };

    let url = this.global.url + `/api/getClubs/` + this.id;

    if (this.platform.is('cordova')) {
      this.nativeHttp.get(url, {}, headers)
        .then(data => {


          this.club = JSON.parse(data['data']);

          this.hideLoadingHandler();

        }, error => {
          console.log(error);
        });
    }
    else {
      this.http.get(url, { headers: headers })
        .subscribe(data => {

          this.club = data;

          this.hideLoadingHandler();

        }, error => {
          console.log(error);
        });
    }
  }

}
