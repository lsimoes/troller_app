import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Loja2Page } from './loja2.page';

const routes: Routes = [
  {
    path: '',
    component: Loja2Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Loja2PageRoutingModule {}
