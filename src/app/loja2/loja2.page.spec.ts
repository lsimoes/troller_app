import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Loja2Page } from './loja2.page';

describe('Loja2Page', () => {
  let component: Loja2Page;
  let fixture: ComponentFixture<Loja2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Loja2Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Loja2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
