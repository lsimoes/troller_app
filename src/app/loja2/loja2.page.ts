import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../global.service';
import { Platform } from '@ionic/angular';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-loja2',
  templateUrl: './loja2.page.html',
  styleUrls: ['./loja2.page.scss'],
})
export class Loja2Page implements OnInit {

  subcategories: any;
  id: any;
  loading: boolean= false;
  qtdNotifications: any;

  constructor(
    private global: GlobalService,
    private platform: Platform,
    private http: HttpClient,
    private nativeHttp: HTTP,
    private loadingController: LoadingController,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {

    this.qtdNotifications = localStorage.getItem('qtdNotificationsBadge');
    setInterval(() => { 
       this.qtdNotifications = localStorage.getItem('qtdNotificationsBadge');
    }, 1000);

    this.route.paramMap.subscribe(params => {
      this.id = params.get("id");

      this.id = this.id - 1;
    })

    let headers = {
      'Content-Type': 'application/json',
      'Authorization': localStorage.getItem('token_type') + ' ' + localStorage.getItem('access_token')
    };

    let url = this.global.urlEp + '/api/products/categories';

    if (this.platform.is('cordova')) {
      this.nativeHttp.get(url, {}, headers)
        .then(res2 => {

          this.loading = !this.loading;

          let data = JSON.parse(res2['data']);

          this.subcategories = data['data'][this.id]['subcategories'];

        }, error => {
          console.log(error);
        });
    }
    else {
      this.http.get(url, { headers: headers })
        .subscribe(data2 => {

          this.loading = !this.loading;

          this.subcategories = data2['data'][this.id]['subcategories'];

        }, error => {
          console.log(error);
        });
    }
  }
  

}
