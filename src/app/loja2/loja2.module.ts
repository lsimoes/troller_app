import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Loja2PageRoutingModule } from './loja2-routing.module';

import { Loja2Page } from './loja2.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Loja2PageRoutingModule
  ],
  declarations: [Loja2Page]
})
export class Loja2PageModule {}
