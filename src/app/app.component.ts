import { Component, OnInit } from '@angular/core';

import { Platform, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { HTTP } from '@ionic-native/http/ngx';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';

import { Router } from '@angular/router';

import { GlobalService } from './global.service';

import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';

import { OneSignal } from '@ionic-native/onesignal/ngx';

import { Badge } from '@ionic-native/badge/ngx';

import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  public selectedIndex = 0;
  logged: any; 

  public appPages = [
    {
      title: 'Início',
      url: '/home',
      icon: 'icon-inicio.svg'
    },
    {
      title: 'Mundo Troller',
      url: '/mundo-troller/null/null',
      icon: 'icon-mundotroller.svg'
    },
    {
      title: 'Experiências',
      url: '/experiencias',
      icon: 'icon-experiencias.svg'
    },
    {
      title: 'Mapas',
      url: '/mapas',
      icon: 'icon-mapas.svg'
    },
    {
      title: 'Loja',
      url: '/loja',
      icon: 'icon-loja.svg'
    },
    {
      title: 'Bio Troller',
      url: '/bio',
      icon: 'icon-biotroller.svg'
    },
    {
      title: 'Clubes',
      url: '/clubes',
      icon: 'icon-clubes.svg'
    },
    {
      title: 'Agenda',
      url: '/agenda',
      icon: 'icon-agenda.svg'
    },
    {
      title: 'Benefícios',
      url: '/beneficios',
      icon: 'icon-beneficios.svg'
    },
    {
      title: 'Concessionárias',
      url: '/concessionarias',
      icon: 'icon-concessionarias.svg'
    }
  ];

  public appPages2 = [
    {
      title: 'Termos de uso',
      url: '/termos',
      icon: 'icon-termos.svg'
    },
    {
      title: 'Sobre o app',
      url: '/sobre',
      icon: 'icon-sobre.svg'
    }
  ];
  // public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];

  requestHeaders = new HttpHeaders().set('Content-Type', 'application/json');
  loading: any;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private http: HttpClient,
    private nativeHttp: HTTP,
    private router: Router,
    private loadingController: LoadingController,
    private global: GlobalService,
    private androidPermissions: AndroidPermissions,
    private oneSignal: OneSignal,
    private alertCtrl: AlertController,
    private storage: Storage
  ) {
    this.initializeApp();
  }

  async showLoadingHandler() {
    if (this.loading == null) {
      this.loading = await this.loadingController.create({
        message: '',
      });
      this.loading.present();
    }
  }

  public hideLoadingHandler() {
    if (this.loading != null) {
        this.loading.dismiss();
        this.loading = null;
    }
  }

  sair()
    {
      localStorage.clear();
      this.router.navigate(['']);
      
    }

  initializeApp() {
    this.platform.ready().then(() => {

      this.androidPermissions.requestPermissions(
        [
          this.androidPermissions.PERMISSION.CAMERA, 
          this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, 
          this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE
        ]
      );

      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.global.getNotificationsCount();

      if(this.platform.is('cordova')) {
        this.setupPush();
      }
    });
  }

  setupPush() {
    // I recommend to put these into your environment.ts
    this.oneSignal.startInit('c08b2ebd-2092-413d-800b-f603b78c339e', '643041562435');
 
    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
 
    // Notifcation was received in general
    this.oneSignal.handleNotificationReceived().subscribe(data => {
      let msg = data.payload.body;
      let title = data.payload.title;

      let qtdBadge = parseInt(localStorage.getItem('qtdNotificationsBadge'));
      let count = qtdBadge + 1;

      localStorage.setItem('qtdNotificationsBadge', count.toString());
      // let additionalData = data.payload.additionalData;
      // this.showAlert(title, msg, additionalData.task);
    });
 
    // Notification was really clicked/opened
    this.oneSignal.handleNotificationOpened().subscribe(data => {
      // Just a note that the data is a different place here!
      // let additionalData = data.notification.payload.additionalData;
 
      // this.showAlert('Notification opened', 'You already read this before', additionalData.task);


    });
 
    this.oneSignal.endInit();
  }
 
  async showAlert(title, msg, task) {
    const alert = await this.alertCtrl.create({
      header: title,
      subHeader: msg,
      buttons: [
        {
          text: `Action: ${task}`,
          handler: () => {
            // E.g: Navigate to a specific screen
          }
        }
      ]
    })
    alert.present();
  }

  



  ngOnInit() {
    // this.showLoadingHandler();

    // const path = window.location.pathname.split('folder/')[1];
    // if (path !== undefined) {
    //   this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    // }

  

  }
}
