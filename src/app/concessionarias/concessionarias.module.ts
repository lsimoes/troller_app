import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConcessionariasPageRoutingModule } from './concessionarias-routing.module';

import { ConcessionariasPage } from './concessionarias.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConcessionariasPageRoutingModule
  ],
  declarations: [ConcessionariasPage]
})
export class ConcessionariasPageModule {}
