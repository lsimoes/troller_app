import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConcessionariasPage } from './concessionarias.page';

const routes: Routes = [
  {
    path: '',
    component: ConcessionariasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConcessionariasPageRoutingModule {}
