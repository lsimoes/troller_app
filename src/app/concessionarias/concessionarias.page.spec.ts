import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ConcessionariasPage } from './concessionarias.page';

describe('ConcessionariasPage', () => {
  let component: ConcessionariasPage;
  let fixture: ComponentFixture<ConcessionariasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConcessionariasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ConcessionariasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
