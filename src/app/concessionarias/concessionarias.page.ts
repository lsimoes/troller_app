import { Component, OnInit } from '@angular/core';
import * as Leaflet from 'leaflet';
import { GlobalService } from '../global.service';
import { Platform } from '@ionic/angular';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
import { ActivatedRoute } from "@angular/router";


@Component({
  selector: 'app-concessionarias',
  templateUrl: './concessionarias.page.html',
  styleUrls: ['./concessionarias.page.scss'],
})
export class ConcessionariasPage implements OnInit {

  map: Leaflet.Map;
  marker: any;
  status: any;
  markers: any = [];
  qtdNotifications: any;

  constructor(
    private global: GlobalService,
    private platform: Platform,
    private http: HttpClient,
    private nativeHttp: HTTP,
    private loadingController: LoadingController,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.qtdNotifications = localStorage.getItem('qtdNotificationsBadge');
    setInterval(() => { 
       this.qtdNotifications = localStorage.getItem('qtdNotificationsBadge');
    }, 1000);
    
      let headers = {
        'Content-Type': 'application/json',
        'Authorization': localStorage.getItem('token_type') + ' ' + localStorage.getItem('api_access_token')
      };

      let headers2 = {
        'Content-Type': 'application/json',
        'Authorization': localStorage.getItem('token_type') + ' ' + localStorage.getItem('access_token')
      };

      let url = this.global.url + `/api/checkDealer/` + localStorage.getItem('user_id');

      let url2 = this.global.urlEp + '/api/dealers';

      if (this.platform.is('cordova')) {

        this.nativeHttp.get(url, {}, headers)
        .then(data => {
          this.status = JSON.parse(data['data']);
        }, error => {
          console.log(error);
        });

        //Latitudes e Longitudes (mapa geral)
        this.nativeHttp.get(url2, {}, headers2)
          .then(data => {
            let dealers = JSON.parse(data['data']);
            let countDealers = dealers['data'];
            let n = countDealers.length;
            for (var i = 0; i < n; i++) {
              this.markers.push([dealers['data'][i]['lat'], dealers['data'][i]['lng']]);
            }
            this.leafletMap();
          }, error => {
            console.log(error);
        });
      }
      else {
        this.http.get(url, { headers: headers })
        .subscribe(data => {
          this.status = data;
        }, error => {
          console.log(error);
        });

        // Latitudes e Longitudes (mapa geral)
        this.http.get(url2, { headers: headers2 })
        .subscribe(data => {
          let dealers = data['data'];
          let n = dealers.length;
          for (var i = 0; i < n; i++) {
            this.markers.push([dealers[i]['lat'], dealers[i]['lng']]);
          }
          this.leafletMap();
        }, error => {
          console.log(error);
        });

      }
  }

  // ionViewDidEnter() { this.leafletMap(); }

  leafletMap() {
    
    this.map = Leaflet.map('mapId').setView([-23.6821604, -46.8754936], 4);
    Leaflet.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
      attribution: '',
      maxZoom: 20,
      minZoom: 4,
      id: 'mapbox/streets-v11',
      tileSize: 512,
      zoomOffset: -1,
      accessToken: 'pk.eyJ1IjoiYWczZGlnaXRhbCIsImEiOiJja2NmMXgxbzQwZTF5MzVueHl6eHgxYXcyIn0.VONM9e7kf8sPGmRbXrUb9w'
    }).addTo(this.map);

    var attribution = this.map.attributionControl;
    attribution.setPrefix('');

    var myIcon = Leaflet.icon({
        iconUrl: 'assets/images/marker.png',
        shadowUrl: 'assets/images/shadow.png',
    });


    // this.markers = [
    //   [-23.675587, -46.698087],
    //   [-23.580012649289927, -46.65038483141511],
    // ];

    // alert(JSON.stringify(this.markers))

    var n = this.markers.length;

    for (var i = 0; i < n; i++) {
      this.marker = Leaflet.marker(this.markers[i], {icon: myIcon});
      this.marker.addTo(this.map);
    }

    let mape = this.map;
    
    setInterval(function () {
      mape.invalidateSize();
   }, 1000);
  }

  ionViewWillLeave() {
    this.map.remove();
  }
  

}
