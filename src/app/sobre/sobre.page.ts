import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { LoadingController, Platform } from '@ionic/angular';
import { GlobalService } from '../global.service';
import { HTTP } from '@ionic-native/http/ngx';


@Component({
  selector: 'app-sobre',
  templateUrl: './sobre.page.html',
  styleUrls: ['./sobre.page.scss'],
})
export class SobrePage implements OnInit {

  content: any;
  loading: any;

  constructor(
    private http: HttpClient,
    private global: GlobalService,
    private loadingController: LoadingController,
    private platform: Platform,
    private nativeHttp: HTTP
  ) { }

  async ionViewWillEnter() {
    this.loading = await this.loadingController.create({
      message: '',
    });
  } 

  ngOnInit() {
    let headers = {
      'Content-Type': 'application/json',
      'Authorization': localStorage.getItem('token_type') + ' ' + localStorage.getItem('api_access_token')
    };

    let url = this.global.url + `/api/getTexts/about`;

    if (this.platform.is('cordova')) {
      this.nativeHttp.get(url, {}, headers)
        .then(res => {

          let data = JSON.parse(res['data']);

          this.content = data[0]['text'];
          this.loading = !this.loading;
        }, error => {
          console.log(error);
        });
    }
    else {
      this.http.get(url, { headers: headers })
        .subscribe(data => {
          this.content = data[0]['text'];
          this.loading = !this.loading;
        }, error => {
          console.log(error);
        });

    }
  }

}
