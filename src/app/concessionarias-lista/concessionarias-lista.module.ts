import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConcessionariasListaPageRoutingModule } from './concessionarias-lista-routing.module';

import { ConcessionariasListaPage } from './concessionarias-lista.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConcessionariasListaPageRoutingModule
  ],
  declarations: [ConcessionariasListaPage]
})
export class ConcessionariasListaPageModule {}
