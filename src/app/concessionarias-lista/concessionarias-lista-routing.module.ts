import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConcessionariasListaPage } from './concessionarias-lista.page';

const routes: Routes = [
  {
    path: '',
    component: ConcessionariasListaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConcessionariasListaPageRoutingModule {}
