import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ConcessionariasListaPage } from './concessionarias-lista.page';

describe('ConcessionariasListaPage', () => {
  let component: ConcessionariasListaPage;
  let fixture: ComponentFixture<ConcessionariasListaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConcessionariasListaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ConcessionariasListaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
