import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../global.service';
import { Platform } from '@ionic/angular';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-evento',
  templateUrl: './evento.page.html',
  styleUrls: ['./evento.page.scss'],
})
export class EventoPage implements OnInit {

  id: any;
  name: any;
  thumb: any;
  eventId: any;
  loading: any;
  guide: any;
  adhesiveness: any;
  qtdNotifications: any;

  constructor(
    private global: GlobalService,
    private platform: Platform,
    private http: HttpClient,
    private nativeHttp: HTTP,
    private loadingController: LoadingController,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  async showLoadingHandler() {
    if (this.loading == null) {
      this.loading = await this.loadingController.create({
        message: '',
      });
      this.loading.present();
    }
  } 

  public hideLoadingHandler() {
    if (this.loading != null) {
        this.loading.dismiss();
        this.loading = null;
    }
  }
  

  ngOnInit() {
    this.showLoadingHandler();

    this.qtdNotifications = localStorage.getItem('qtdNotificationsBadge');
    setInterval(() => { 
       this.qtdNotifications = localStorage.getItem('qtdNotificationsBadge');
    }, 1000);

    this.route.paramMap.subscribe(params => {
      this.id = params.get("id");
    })

    let headers = {
      'Content-Type': 'application/json',
      'Authorization': localStorage.getItem('token_type') + ' ' + localStorage.getItem('access_token')
    };

    let url = this.global.urlEp + '/api/events/details/' + this.id;

    if (this.platform.is('cordova')) {
      this.nativeHttp.get(url, {}, headers)
        .then(res2 => {


          let data = JSON.parse(res2['data']);

          this.name = data['data']['name'];
          this.thumb = data['data']['highlight_img_url'];
          this.eventId = data['data']['id'];
          this.guide = data['data']['guide'];
          this.adhesiveness = data['data']['adhesiveness'];

          this.hideLoadingHandler();

        }, error => {
          console.log(error);
        });
    }
    else {
      this.http.get(url, { headers: headers })
        .subscribe(data2 => {

          this.name = data2['data']['name'];
          this.thumb = data2['data']['highlight_img_url'];
          this.eventId = data2['data']['id'];
          this.guide = data2['data']['guide'];
          this.adhesiveness = data2['data']['adhesiveness'];

          this.hideLoadingHandler();

        }, error => {
          console.log(error);
        });
    }
  }

}
