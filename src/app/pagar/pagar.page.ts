import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../global.service';
import { Platform } from '@ionic/angular';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
import { ActivatedRoute } from "@angular/router";
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-pagar',
  templateUrl: './pagar.page.html',
  styleUrls: ['./pagar.page.scss'],
})
export class PagarPage implements OnInit {

  amount: any;
  idCategory: any;
  role1: any;
  role2: any;
  idVehicle: any;
  participants: any;
  value: any;

  loading: any;

  headers = {
    'Content-Type': 'application/json',
    'Authorization': localStorage.getItem('token_type') + ' ' + localStorage.getItem('access_token')
  };

  headersPaypal = {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer' + ' ' + localStorage.getItem('paypal_access_token')
  };

  constructor(
    private global: GlobalService,
    private platform: Platform,
    private http: HttpClient,
    private nativeHttp: HTTP,
    private loadingController: LoadingController,
    private router: Router,
    private route: ActivatedRoute,
    public alertController: AlertController,
  ) { }

  async showLoadingHandler() {
    if (this.loading == null) {
      this.loading = await this.loadingController.create({
        message: '',
      });
      this.loading.present();
    }
  } 

  public hideLoadingHandler() {
    if (this.loading != null) {
        this.loading.dismiss();
        this.loading = null;
    }
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      // this.idEvent = params.get("idEvent");
      this.idCategory = params.get("idCategory");
      this.role1 = params.get("role1");
      this.role2 = params.get("role2");
      this.idVehicle = params.get("idVehicle");
      this.participants = params.get("participants");
      this.value = params.get("value");
    })
    
    this.amount = this.participants * this.value;

    let postData = {
      "grant_type": 'client_credentials',
    }

    let headers = {
      'Authorization': 'Basic ' + btoa('AUvYVYHqfX_rZP8ryuOvcATnNmdlS-KN2neP2bMeFv4CLGPW4JPpXWdZ-sO0-D-FJoDmRontVvVZAkM9:EFxLb1kJEfVnSgVdxeoj1nIPmQP3qnC57gC1w54NTldoX_JQ3T86Jdp8FhTXwI-0FWmQmFsKgLdgY6Hq'),
      'Accept': 'application/json',
      'Content-Type':'application/x-www-form-urlencoded'
    };

    let url = 'https://api-m.sandbox.paypal.com/v1/oauth2/token';

    if (this.platform.is('cordova')) {
      this.nativeHttp.post(url, "grant_type=client_credentials", headers)
        .then(res => {

          let data = JSON.parse(res['data']);

          localStorage.setItem('paypal_access_token', data['access_token']);
          
        }, error => {
          alert('Erro na api: ' + JSON.stringify(error['error']));
              this.loading.dismiss();
        });
    }
    else {
      this.http.post(url, "grant_type=client_credentials", { headers: headers })
        .subscribe(data => {
          localStorage.setItem('paypal_access_token', data['access_token']);
        }, error => {
          alert('Erro na api: ' + JSON.stringify(error['error']));
              this.loading.dismiss();
        });
    }


  }

  onClickSubmit(formData) {
    // if (formData.card_holder_name == '' || formData.card_number == '' || formData.card_expiration_month == '' || formData.card_expiration_year == '' || formData.card_cvv == '') 
    // {
    //     this.global.presentAlert('Preencha os campos obrigatórios');
    // }
    // else 
    // {
    //   this.pay();
    // }

    this.pay();
  }

  async pay() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Confirmação!',
      message: 'Deseja confirmar o pagamento?',
      buttons: [
        {
          text: 'Não',
          handler: () => {
            console.log('cancel');
          }
        }, {
          text: 'Sim',
          handler: () => {

            this.showLoadingHandler();
            // let url = 'https://jwt.servershot.com.br/troller/portal/api/events/new-team';

            // let postData = {
            //   "contestant_id": localStorage.getItem('user_id'),
            //   "contest_id": this.idCategory,
            //   "role_responsible_for_vehicle": this.role1,
            //   "role": this.role2,
            //   "contestant_vehicle_id": this.idVehicle,
            //   "max_team_invites": this.participants,
            // }

            let url = 'https://api.sandbox.paypal.com/v2/checkout/orders';

            let postData = {
              "intent": "CAPTURE",
              "purchase_units": [
                {
                  "amount": {
                    "currency_code": "BRL",
                    "value": this.amount
                  }
                }
              ]
            }

            if(this.platform.is('cordova'))
            {
              this.nativeHttp.post(url, postData, this.headersPaypal)
                  .then(res => {
                    
                    this.hideLoadingHandler();
                    this.global.presentAlert('Pagamento confirmado!');
                    // this.router.navigate(['/bio']);
                    
                    this.global.presentAlert(JSON.stringify(res['data']));
                      
                    }, error => {
                      this.hideLoadingHandler();
                      this.global.presentAlert(JSON.stringify(error['error']));
              });
            }
            else
            {
              this.http.post(url, postData, { headers: this.headersPaypal})
                .subscribe(data => {

                  this.hideLoadingHandler();
                  this.global.goLink(data['links'][1]['href']);
                    
                  }, error => {
                    this.hideLoadingHandler();
                    this.global.presentAlert(JSON.stringify(error['error']));
              });
            }
          }
        }
      ]
    });

    await alert.present();
  }

}
