import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../global.service';
import { Platform } from '@ionic/angular';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';

@Component({
  selector: 'app-calendario',
  templateUrl: './calendario.page.html',
  styleUrls: ['./calendario.page.scss'],
})
export class CalendarioPage implements OnInit {

  calendars: any;
  loading: any;
  qtdNotifications: any;

  constructor(
    private global: GlobalService,
    private platform: Platform,
    private http: HttpClient,
    private nativeHttp: HTTP,
    private loadingController: LoadingController,
    private router: Router) { }

    async showLoadingHandler() {
      if (this.loading == null) {
        this.loading = await this.loadingController.create({
          message: '',
        });
        this.loading.present();
      }
    } 
  
    public hideLoadingHandler() {
      if (this.loading != null) {
          this.loading.dismiss();
          this.loading = null;
      }
    }

  ngOnInit() {
    this.showLoadingHandler();

    this.qtdNotifications = localStorage.getItem('qtdNotificationsBadge');
    setInterval(() => { 
       this.qtdNotifications = localStorage.getItem('qtdNotificationsBadge');
    }, 1000);

    let headers = {
      'Content-Type': 'application/json',
      'Authorization': localStorage.getItem('token_type') + ' ' + localStorage.getItem('access_token')
    };

    let url = this.global.urlEp + '/api/events/calendar';

    if (this.platform.is('cordova')) {
      this.nativeHttp.get(url, {}, headers)
        .then(res => {

          let data = JSON.parse(res['data']);

          this.calendars = data['data'];

          this.hideLoadingHandler();

        }, error => {
          console.log(error);
        });
    }
    else {
      this.http.get(url, { headers: headers })
        .subscribe(data => {
          
          // alert(data['data'][0]["name"])

          this.calendars = data['data'];

          this.hideLoadingHandler();
          
        }, error => {
          console.log(error);
        });
    }
  }

}
