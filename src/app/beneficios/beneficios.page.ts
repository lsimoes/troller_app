import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../global.service';
import { Platform } from '@ionic/angular';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
import { ActivatedRoute } from "@angular/router";


@Component({
  selector: 'app-beneficios',
  templateUrl: './beneficios.page.html',
  styleUrls: ['./beneficios.page.scss'],
})
export class BeneficiosPage implements OnInit {

  types: any;
  benefits: any;
  loading: boolean= false;
  qtdNotifications: any;

  constructor(
    private global: GlobalService,
    private platform: Platform,
    private http: HttpClient,
    private nativeHttp: HTTP,
    private loadingController: LoadingController,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {

    this.qtdNotifications = localStorage.getItem('qtdNotificationsBadge');
    setInterval(() => { 
       this.qtdNotifications = localStorage.getItem('qtdNotificationsBadge');
    }, 1000);

    let headers = {
      'Content-Type': 'application/json',
      'Authorization': localStorage.getItem('token_type') + ' ' + localStorage.getItem('api_access_token')
    };

    let url = this.global.url + `/api/getBenefitTypes`;

    if (this.platform.is('cordova')) {
      this.nativeHttp.get(url, {}, headers)
        .then(res => {

          let data = JSON.parse(res['data']);
          
          this.types = data;

        }, error => {
          console.log(error);
        });
    }
    else {
      this.http.get(url, { headers: headers })
        .subscribe(data => {
          
          this.types = data;

          console.log(data);
         }, error => {
          console.log(error);
      });
    }

    this.getBenefits();
  }

  getBenefits(type=null) {
    this.loading = true;

    let headers = {
      'Content-Type': 'application/json',
      'Authorization': localStorage.getItem('token_type') + ' ' + localStorage.getItem('api_access_token')
    };

    let url = this.global.url + `/api/getBenefits/` + type;

    if (this.platform.is('cordova')) {
      this.nativeHttp.get(url, {}, headers)
        .then(res => {

          let data = JSON.parse(res['data']);

          this.loading = false;
          
          this.benefits = data;

        }, error => {
          console.log(error);
        });
    }
    else {
      this.http.get(url, { headers: headers })
        .subscribe(data => {
          this.loading = false;
          
          this.benefits = data;

          console.log(data);
         }, error => {
          console.log(error);
      });
    }
  }

}
