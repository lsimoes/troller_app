import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../global.service';
import { Platform } from '@ionic/angular';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';

@Component({
  selector: 'app-loja',
  templateUrl: './loja.page.html',
  styleUrls: ['./loja.page.scss'],
})
export class LojaPage implements OnInit {

  categories: any;
  loading: boolean= false;
  qtdNotifications: any;

  constructor(
    private global: GlobalService,
    private platform: Platform,
    private http: HttpClient,
    private nativeHttp: HTTP,
    private loadingController: LoadingController,
    private router: Router
  ) { }

  ngOnInit() {
    this.qtdNotifications = localStorage.getItem('qtdNotificationsBadge');
    setInterval(() => { 
       this.qtdNotifications = localStorage.getItem('qtdNotificationsBadge');
    }, 1000);
    
    let headers = {
      'Content-Type': 'application/json',
      'Authorization': localStorage.getItem('token_type') + ' ' + localStorage.getItem('access_token')
    };

    let url = this.global.urlEp + '/api/products/categories';

    if (this.platform.is('cordova')) {
      this.nativeHttp.get(url, {}, headers)
        .then(res => {

          this.loading = !this.loading;

          let data = JSON.parse(res['data']);

          this.categories = data['data'];

          // alert(this.categories)

          // this.returnLogin(data['data']["id"], data['data']["name"], data['data']["nickname"], data['data']["birthdate"], data['data']["cpf"], data['data']["cep"], data['data']["address"], data['data']["number"], data['data']["complement"], data['data']["neighborhood"], data['data']["mobile_number"], data['data']['main_email'][0]['email']);

        }, error => {
          console.log(error);
        });
    }
    else {
      this.http.get(url, { headers: headers })
        .subscribe(data => {
          
          // alert(data['data'][0]["name"])

          this.loading = !this.loading;

          this.categories = data['data'];

          // for(let res of data['data']) {
          //   console.log(res['name']);
          // }


        }, error => {
          console.log(error);
        });
    }
  }

}
