import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CategoriasEventoPageRoutingModule } from './categorias-evento-routing.module';

import { CategoriasEventoPage } from './categorias-evento.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CategoriasEventoPageRoutingModule
  ],
  declarations: [CategoriasEventoPage]
})
export class CategoriasEventoPageModule {}
