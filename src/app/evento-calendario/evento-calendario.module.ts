import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EventoCalendarioPageRoutingModule } from './evento-calendario-routing.module';

import { EventoCalendarioPage } from './evento-calendario.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EventoCalendarioPageRoutingModule
  ],
  declarations: [EventoCalendarioPage]
})
export class EventoCalendarioPageModule {}
