import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EventoCalendarioPage } from './evento-calendario.page';

const routes: Routes = [
  {
    path: '',
    component: EventoCalendarioPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EventoCalendarioPageRoutingModule {}
