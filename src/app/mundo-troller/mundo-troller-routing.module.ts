import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MundoTrollerPage } from './mundo-troller.page';

const routes: Routes = [
  {
    path: '',
    component: MundoTrollerPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MundoTrollerPageRoutingModule {}
