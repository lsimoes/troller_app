import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MundoTrollerPageRoutingModule } from './mundo-troller-routing.module';

import { MundoTrollerPage } from './mundo-troller.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MundoTrollerPageRoutingModule
  ],
  declarations: [MundoTrollerPage]
})
export class MundoTrollerPageModule {}
