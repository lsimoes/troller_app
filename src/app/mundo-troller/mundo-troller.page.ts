import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../global.service';
import { Platform } from '@ionic/angular';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-mundo-troller',
  templateUrl: './mundo-troller.page.html',
  styleUrls: ['./mundo-troller.page.scss'],
})
export class MundoTrollerPage implements OnInit {

  session: any;
  userid: any;
  publications: any;
  loading: boolean= false;
  qtdNotifications: any;

  constructor(
    private global: GlobalService,
    private platform: Platform,
    private http: HttpClient,
    private nativeHttp: HTTP,
    private loadingController: LoadingController,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.qtdNotifications = localStorage.getItem('qtdNotificationsBadge');
    setInterval(() => { 
       this.qtdNotifications = localStorage.getItem('qtdNotificationsBadge');
    }, 1000);

    this.route.paramMap.subscribe(params => {
      this.session = params.get("session");
      this.userid = params.get("userid");
    })

    this.getPublications(this.session, this.userid);
  }

  getPublications(session=null, userid=null) {
    this.loading = true;

    let headers = {
      'Content-Type': 'application/json',
      'Authorization': localStorage.getItem('token_type') + ' ' + localStorage.getItem('api_access_token')
    };

    let url = this.global.url + `/api/getPublications/` + session + `/` + userid;

    if (this.platform.is('cordova')) {
      this.nativeHttp.get(url, {}, headers)
        .then(res => {

          let data = JSON.parse(res['data']);

          this.loading = false;
          
          this.publications = data;

        }, error => {
          console.log(error);
        });
    }
    else {
      this.http.get(url, { headers: headers })
        .subscribe(data => {
          
          this.loading = false;
          
          this.publications = data;

        }, error => {
          console.log(error);
        });
    }
  }

}
