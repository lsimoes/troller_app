import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MundoTrollerPage } from './mundo-troller.page';

describe('MundoTrollerPage', () => {
  let component: MundoTrollerPage;
  let fixture: ComponentFixture<MundoTrollerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MundoTrollerPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MundoTrollerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
