import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { NavController } from '@ionic/angular';
import { Platform } from '@ionic/angular';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { AlertController } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';

//Chaves OAuth
// const CLIENT_ID = 3;
// const CLIENT_SECRET = 'K6PqQjpKQcXgOLfHx0KyU4bp60TIC9OhysN1ivBP';
// const REDIRECT_URL = '';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  // public url = "https://troller.com.br";

  public url = "http://phplaravel-359095-1431173.cloudwaysapps.com";
  public urlEp = "https://www.troller.com.br";

  // public url = "http://localhost:8000";

  user_id = localStorage.getItem('user_id');
  
  readed: any;
  notifications: any;
  count: any;

  options : InAppBrowserOptions = {
    location : 'yes',//Or 'no' 
    hidden : 'no', //Or  'yes'
    clearcache : 'yes',
    clearsessioncache : 'yes',
    zoom : 'yes',//Android only ,shows browser zoom controls 
    hardwareback : 'yes',
    mediaPlaybackRequiresUserAction : 'no',
    shouldPauseOnSuspend : 'no', //Android only 
    closebuttoncaption : 'Close', //iOS only
    disallowoverscroll : 'no', //iOS only 
    toolbar : 'yes', //iOS only 
    enableViewportScale : 'no', //iOS only 
    allowInlineMediaPlayback : 'no',//iOS only 
    presentationstyle : 'pagesheet',//iOS only 
    fullscreen : 'yes',//Windows only    
};

  constructor(
    private router: Router,
    private socialSharing: SocialSharing,
    private http: HttpClient,
    private navCtrl: NavController,
    public platform: Platform,
    private iab: InAppBrowser,
    public alertController: AlertController,
    private nativeHttp: HTTP,
  ) { }

  public backButton(){
    this.navCtrl.back();
  }

  public sair()
    {
      localStorage.clear();
      this.router.navigate(['']);
      
    }

  public validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  

  goLink(link) {
	  const browser = this.iab.create(link,'_system', this.options);
	  browser.show();
  }
  
  public shareSocial(file=null, title=null, description=null) {
    if(file == null)
    {
        file = this.url + '/storage/troller.png';
    }

    if(description == null)
    {
      description = '';
    }
    this.socialSharing.share(title + ' - ' + description,'Venha para o Mundo Troller.', file, 'https://mundotroller.com.br/');
  }

  async presentAlert(message) {
    const alert = await this.alertController.create({
        message: message,
        buttons: ['OK']
    });

    await alert.present();
  }

  public getNotificationsCount() {
    let headersOS = {
      'Content-Type': 'application/json',
      'Authorization': 'Basic NmU3MjkyMjItMzg0OS00NDY2LWJjODYtOTNlYWU2M2UwZjk3'
    };

    let url = 'https://onesignal.com/api/v1/notifications?app_id=c08b2ebd-2092-413d-800b-f603b78c339e&limit=50&offset=:offset&kind=:kind';

    if (this.platform.is('cordova')) {
      this.nativeHttp.get(url, {}, headersOS)
        .then(res => {

          let data = JSON.parse(res['data']);

          this.readed = parseInt(localStorage.getItem('readedNotifications'));
          if(!this.readed) {
            this.readed = 0;
          }

          this.notifications = data['notifications'].length;

          if(this.readed > this.notifications) {
            this.count = 0;
            localStorage.setItem('readedNotifications', '0');
          }
          else {
            this.count = this.notifications - this.readed;
          }
          
          

          // alert(this.notifications);
          // alert(this.readed)

          localStorage.setItem('qtdNotificationsOS', this.notifications);
          localStorage.setItem('qtdNotificationsBadge', this.count.toString());

        }, error => {
          console.log(error);
        });
    }
    else {
      this.http.get(url, { headers: headersOS })
        .subscribe(data => {

          this.readed = parseInt(localStorage.getItem('readedNotifications'));
          if(!this.readed) {
            this.readed = 0;
          }

          this.notifications = data['notifications'].length;

          if(this.readed > this.notifications) {
            this.count = 0;
            localStorage.setItem('readedNotifications', '0');
          }
          else {
            this.count = this.notifications - this.readed;
          }
          
          

          // alert(this.notifications);
          // alert(this.readed)

          localStorage.setItem('qtdNotificationsOS', this.notifications);
          localStorage.setItem('qtdNotificationsBadge', this.count.toString());

        }, error => {
          console.log(error);
        });
    }
  }
  
}
