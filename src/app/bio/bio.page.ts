import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../global.service';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
import { AlertController } from '@ionic/angular';

import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { ActionSheetController, Platform } from '@ionic/angular';
import {
  MediaCapture,
  MediaFile,
  CaptureError
} from '@ionic-native/media-capture/ngx';
import { File, FileEntry } from '@ionic-native/File/ngx';
import { Media, MediaObject } from '@ionic-native/media/ngx';
import { StreamingMedia } from '@ionic-native/streaming-media/ngx';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';

import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';

import { Storage } from '@ionic/storage';

const MEDIA_FOLDER_NAME = 'files_troller';

@Component({
  selector: 'app-bio',
  templateUrl: './bio.page.html',
  styleUrls: ['./bio.page.scss'],
})
export class BioPage implements OnInit {

  user_id: any;
  name: any;
  nickname: any;
  birthdate: any;
  cpf: any;
  cep: any;
  address: any;
  city_id: any;
  number: any;
  complement: any;
  neighborhood: any;
  mobile_number: any;
  email: any;
  loading: any;

  vehicles: any;
  events: any;
  states: any;
  cities: any;

  files = [];
  medias = [];
  datenow: any;
  filedata:any;
  filedata0: any;
  filedata0name: any;
  extFile: any;

  avatar: any;
  notifications: any;
  
  headers = {
    'Content-Type': 'application/json',
    'Authorization': localStorage.getItem('token_type') + ' ' + localStorage.getItem('access_token')
  };
  

  constructor(
    private global: GlobalService,
    private platform: Platform,
    private http: HttpClient,
    private nativeHttp: HTTP,
    private loadingController: LoadingController,
    private router: Router,
    public alertController: AlertController,
    private imagePicker: ImagePicker,
    private mediaCapture: MediaCapture,
    private file: File,
    private media: Media,
    private photoViewer: PhotoViewer,
    private plt: Platform,
    private transfer: FileTransfer,
    private streamingMedia: StreamingMedia,
    private actionSheetController: ActionSheetController,
    private storage: Storage
  ) { }

  

  async showLoadingHandler() {
    if (this.loading == null) {
      this.loading = await this.loadingController.create({
        message: '',
      });
      this.loading.present();
    }
  } 

  public hideLoadingHandler() {
    if (this.loading != null) {
        this.loading.dismiss();
        this.loading = null;
    }
  }

  async deleteVehicle(id) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Confirmação!',
      message: 'Deseja excluir este veículo?',
      buttons: [
        {
          text: 'Não',
          handler: () => {
            console.log('cancel');
          }
        }, {
          text: 'Sim',
          handler: () => {
            let url = this.global.urlEp + '/api/vehicles/delete';

            let postData = {
              "id": id,
            }

            if(this.platform.is('cordova'))
            {
              this.nativeHttp.post(url, postData, this.headers)
                  .then(res => {
                    
                    this.global.presentAlert('Veículo Deletado.');
                    this.getVehicles();
                      
                    }, error => {
                      console.log(error);
              });
            }
            else
            {
              this.http.post(url, postData, { headers: this.headers})
                .subscribe(data => {

                  this.global.presentAlert('Veículo Deletado.');
                    this.getVehicles();
                    
                  }, error => {
                  console.log(error);
              });
            }
          }
        }
      ]
    });

    await alert.present();
  }

  ngOnInit() {

    localStorage.setItem('readedNotifications', localStorage.getItem('qtdNotificationsOS'));
    localStorage.setItem('qtdNotificationsBadge', '0');


    this.user_id = localStorage.getItem('user_id');
    this.name = localStorage.getItem('name');
    this.nickname = ((localStorage.getItem('nickname') == 'null') ? '' : localStorage.getItem('nickname'));
    this.birthdate = localStorage.getItem('birthdate');
    this.cpf = localStorage.getItem('cpf');
    this.cep = ((localStorage.getItem('cep') == 'null') ? '' : localStorage.getItem('cep'));
    this.address = ((localStorage.getItem('address') == 'null') ? '' : localStorage.getItem('address'));
    this.number = ((localStorage.getItem('number') == 'null') ? '' : localStorage.getItem('number'));
    this.complement = ((localStorage.getItem('complement') == 'null') ? '' : localStorage.getItem('complement'));
    this.city_id = ((localStorage.getItem('city_id') == 'null') ? null : localStorage.getItem('city_id'));
    this.neighborhood = localStorage.getItem('neighborhood');
    this.mobile_number = ((localStorage.getItem('mobile_number') == 'null') ? '' : localStorage.getItem('mobile_number'));
    this.email = localStorage.getItem('email');

    this.getUser();
    this.getVehicles();
    this.getEvents();
    this.getStates();
    this.getNotifications();

    // this.storage.get('msg').then((val) => {
    //   alert(val);
    // });
  }

  getUser() {
    let headers = {
      'Content-Type': 'application/json',
      'Authorization': localStorage.getItem('token_type') + ' ' + localStorage.getItem('api_access_token')
    };

    let url = this.global.url + `/api/getUser/` + this.user_id;

    if (this.platform.is('cordova')) {
      this.nativeHttp.get(url, {}, headers)
        .then(data => {

          let user = JSON.parse(data['data']);
          
          this.avatar = this.global.url + "/storage/" + user[0]['avatar'];

          // alert(this.avatar);

          this.hideLoadingHandler();

        }, error => {
          console.log(error);
        });
    }
    else {
      this.http.get(url, { headers: headers })
        .subscribe(data => {

          this.avatar = this.global.url + "/storage/" + data[0]['avatar'];

          this.hideLoadingHandler();

        }, error => {
          console.log(error);
        });
    }
  }

  getStates() {
    let url = this.global.urlEp + '/api/locations/states';

    if (this.platform.is('cordova')) {
      this.nativeHttp.get(url, {}, this.headers)
        .then(res => {

          let data = JSON.parse(res['data']);

          this.states = data['data'];

        }, error => {
          console.log(error);
        });
    }
    else {
      this.http.get(url, { headers: this.headers })
        .subscribe(data => {

          this.states = data['data'];

        }, error => {
          console.log(error);
        });
    }
    
  }

  getCities(stateId) {

    let url = this.global.urlEp + '/api/locations/cities?filters[state_id]=' + stateId;

    if (this.platform.is('cordova')) {
      this.nativeHttp.get(url, {}, this.headers)
        .then(res => {

          let data = JSON.parse(res['data']);

          this.cities = data['data'];

        }, error => {
          console.log(error);
        });
    }
    else {
      this.http.get(url, { headers: this.headers })
        .subscribe(data => {

          this.cities = data['data'];

        }, error => {
          console.log(error);
        });
    }
    
  }

  getVehicles() {

    let url = this.global.urlEp + '/api/contestants/'+this.user_id+'/vehicles';

    if (this.platform.is('cordova')) {
      this.nativeHttp.get(url, {}, this.headers)
        .then(res => {

          let data = JSON.parse(res['data']);

          this.vehicles = data['data'];

        }, error => {
          console.log(error);
        });
    }
    else {
      this.http.get(url, { headers: this.headers })
        .subscribe(data => {

          this.vehicles = data['data'];

        }, error => {
          console.log(error);
        });
    }
  }

  getEvents() {
    let url = this.global.urlEp + '/api/contestants/'+this.user_id+'/events';

    if (this.platform.is('cordova')) {
      this.nativeHttp.get(url, {}, this.headers)
        .then(res => {

          let data = JSON.parse(res['data']);

          this.events = data['data'];

        }, error => {
          console.log(error);
        });
    }
    else {
      this.http.get(url, { headers: this.headers })
        .subscribe(data => {

          this.events = data['data'];

        }, error => {
          console.log(error);
        });
    }
  }

  getNotifications() {
    let headersOS = {
      'Content-Type': 'application/json',
      'Authorization': 'Basic NmU3MjkyMjItMzg0OS00NDY2LWJjODYtOTNlYWU2M2UwZjk3'
    };

    let url = 'https://onesignal.com/api/v1/notifications?app_id=c08b2ebd-2092-413d-800b-f603b78c339e&limit=50&offset=:offset&kind=:kind';

    if (this.platform.is('cordova')) {
      this.nativeHttp.get(url, {}, headersOS)
        .then(res => {

          let data = JSON.parse(res['data']);

          this.notifications = data['notifications'];

        }, error => {
          console.log(error);
        });
    }
    else {
      this.http.get(url, { headers: headersOS })
        .subscribe(data => {

          this.notifications = data['notifications'];

        }, error => {
          console.log(error);
        });
    }
  }

  loadFiles() {
    this.file.listDir(this.file.dataDirectory, MEDIA_FOLDER_NAME).then(
      res => {
        this.files = res;
      },
      err => console.log('error loading files: ', err)
    );
  }

  async selectMedia() {

    const actionSheet = await this.actionSheetController.create({
      header: 'Selecione a opção desejada.',
      buttons: [
        {
          text: 'Tirar foto',
          handler: () => {
            this.captureImage();
          }
        },
        {
          text: 'Escolher do celular',
          handler: () => {
            this.pickImages();
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel'
        }
      ]
    });
    await actionSheet.present();
    
  }

  pickImages() {
    this.imagePicker.getPictures({}).then(
      results => {
        for (var i = 0; i < results.length; i++) {
          this.copyFileToLocalDir(results[i]);
        }
      }
    );
  }

  captureImage() {
    this.mediaCapture.captureImage().then(
      (data: MediaFile[]) => {
        if (data.length > 0) {
          this.copyFileToLocalDir(data[0].fullPath);
        }
      },
      (err: CaptureError) => console.error(err)
    );
  }

  copyFileToLocalDir(fullPath) {

    this.showLoadingHandler();

    let myPath = fullPath;
    // Make sure we copy from the right location
    if (fullPath.indexOf('file://') < 0) {
      myPath = 'file://' + fullPath;
    }

    const ext = myPath.split('.').pop();
    const d = Date.now();
    const newName = `${d}.${ext}`;

    this.extFile = ext;

    const name = myPath.substr(myPath.lastIndexOf('/') + 1);
    const copyFrom = myPath.substr(0, myPath.lastIndexOf('/') + 1);
    const copyTo = this.file.dataDirectory + MEDIA_FOLDER_NAME;

    this.filedata0 = copyTo;
    this.filedata0name = newName;

    this.medias.push({ name: this.filedata0name, uri: myPath});

    let fileTransfer: FileTransferObject = this.transfer.create();

    let options: FileUploadOptions = {
      fileKey: 'media0',
      fileName: this.filedata0name,
      mimeType: 'multipart/form-data',
      headers: {},
      params: {
        'userId': this.user_id
      }
    }
    
    fileTransfer.upload(myPath, this.global.url+'/api/upload-avatar', options)
    .then((data) => {
      this.hideLoadingHandler();
      // this.loadFiles();
      this.files = this.medias;
      this.avatar = this.global.url + "/storage/" + JSON.stringify(data["response"]);
    }, (err) => {
      alert('erro no upload');
      alert(JSON.stringify(err));
      this.hideLoadingHandler();
    })

  }

  openFile(f: FileEntry) {
    if (f.name.indexOf('.wav') > -1) {
      // We need to remove file:/// from the path for the audio plugin to work
      const path = f.nativeURL.replace(/^file:\/\//, '');
      const audioFile: MediaObject = this.media.create(path);
      audioFile.play();
    } else if (f.name.indexOf('.MOV') > -1 || f.name.indexOf('.mp4') > -1) {
      // E.g: Use the Streaming Media plugin to play a video
      this.streamingMedia.playVideo(f.nativeURL);
    } else if (f.name.indexOf('.jpg') > -1) {
      // E.g: Use the Photoviewer to present an Image
      this.photoViewer.show(f.nativeURL, 'Foto');
    }
  }

  deleteFile(f: FileEntry) {
    const path = f.nativeURL.substr(0, f.nativeURL.lastIndexOf('/') + 1);
    this.file.removeFile(path, f.name).then(() => {

      var fileObj = this.medias.find(x => x.name == f.name);

      this.medias.splice(this.medias.indexOf(fileObj), 1)

      this.loadFiles();
    }, err => console.log('error remove: ', err));
  }

  onClickSubmit(formData) {
    if (formData.name == '' || formData.cpf == '' || formData.email == '' || formData.mobile_number == '' || formData.birthdate == '' || formData.cep == '' || formData.address == '' || formData.number == '' || formData.complement == '' || formData.city_id == '') {
        this.global.presentAlert('Preencha os campos obrigatórios.');
    }
    // else if (formData.password != '' || formData.password_confirmation != '') {
    //   if (formData.password.length < 6) {
    //     this.global.presentAlert('A senha precisa ter no mínimo 6 caracteres.');
    //   }
    //   else if(formData.password != formData.password_confirmation) {
    //     this.global.presentAlert('Confirmação de senha não corresponde.');
    //   }
    // }
    else {
      if(this.global.validateEmail(formData.email))
      {
        this.showLoadingHandler();

        

        let postData = {
          "contestant_id": localStorage.getItem('user_id'),
          "name": formData.name,
          "nickname": formData.nickname,
          "cpf": formData.cpf,
          "email": formData.email,
          "mobile_number": formData.mobile_number,
          "birthyear": formData.birthyear,
          "cep": formData.cep,
          "address": formData.address,
          "number": formData.number,
          "complement": formData.complement,
          "city_id": formData.city_id,
        }

        if (formData.password != '' && formData.password_confirmation != '') {
          let postData2 = {
            "password": formData.password,
            "password_confirmation": formData.password_confirmation,
          }

          postData = Object.assign(postData, postData2);

        }
        

        let url = this.global.urlEp + '/api/contestants/update';

        if(this.platform.is('cordova'))
        {
          this.nativeHttp.post(url, postData, this.headers)
              .then(res => {

                this.hideLoadingHandler();
                this.global.presentAlert('Dados atualizados. Faça seu login novamente.');
                this.global.sair();
                  
              }, error => {
                alert('Erro ao editar conta: ' + JSON.stringify(error['error']['data']));
                this.hideLoadingHandler();
              });
        }
        else
        {
          this.http.post(url, postData, { headers: this.headers})
            .subscribe(data => {
                
              this.hideLoadingHandler();
              this.global.presentAlert('Dados atualizados. Faça seu login novamente.');
              this.global.sair();

              }, error => {
                alert('Erro ao editar conta: ' + JSON.stringify(error['error']['data']));
                this.hideLoadingHandler();
          });
        }
      }
      else {
          this.global.presentAlert('E-mail inválido!');
      }
    }
  }
}
