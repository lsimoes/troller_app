import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PublicarFotovideoPage } from './publicar-fotovideo.page';

const routes: Routes = [
  {
    path: '',
    component: PublicarFotovideoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PublicarFotovideoPageRoutingModule {}
