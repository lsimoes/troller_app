import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PublicarFotovideoPageRoutingModule } from './publicar-fotovideo-routing.module';

import { PublicarFotovideoPage } from './publicar-fotovideo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PublicarFotovideoPageRoutingModule
  ],
  declarations: [PublicarFotovideoPage]
})
export class PublicarFotovideoPageModule {}
