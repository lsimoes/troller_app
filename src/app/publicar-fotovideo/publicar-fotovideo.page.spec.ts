import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PublicarFotovideoPage } from './publicar-fotovideo.page';

describe('PublicarFotovideoPage', () => {
  let component: PublicarFotovideoPage;
  let fixture: ComponentFixture<PublicarFotovideoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicarFotovideoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PublicarFotovideoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
