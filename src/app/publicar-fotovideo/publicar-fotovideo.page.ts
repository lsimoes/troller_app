import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { GlobalService } from '../global.service';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { ActionSheetController, Platform } from '@ionic/angular';
import {
  MediaCapture,
  MediaFile,
  CaptureError
} from '@ionic-native/media-capture/ngx';
import { File, FileEntry } from '@ionic-native/File/ngx';
import { Media, MediaObject } from '@ionic-native/media/ngx';
import { StreamingMedia } from '@ionic-native/streaming-media/ngx';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';

import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';

import { HTTP } from '@ionic-native/http/ngx';

const MEDIA_FOLDER_NAME = 'files_troller';

@Component({
  selector: 'app-publicar-fotovideo',
  templateUrl: './publicar-fotovideo.page.html',
  styleUrls: ['./publicar-fotovideo.page.scss'],
})
export class PublicarFotovideoPage implements OnInit {

  files = [];
  medias = [];
  datenow: any;
  filedata:any;
  filedata0: any;
  filedata0name: any;
  loading: any;
  extFile: any;

  statusDealer: any;
  statusClub: any;

  qtdNotifications: any;

  headers = {
    'Content-Type': 'application/json',
    'Authorization': localStorage.getItem('token_type') + ' ' + localStorage.getItem('api_access_token')
  };

  constructor(
    private router: Router,
    private global: GlobalService,
    private imagePicker: ImagePicker,
    private mediaCapture: MediaCapture,
    private file: File,
    private media: Media,
    private photoViewer: PhotoViewer,
    private plt: Platform,
    private transfer: FileTransfer,
    private loadingController: LoadingController,
    private streamingMedia: StreamingMedia,
    private actionSheetController: ActionSheetController,
    private http: HttpClient,
    private platform: Platform,
    private nativeHttp: HTTP
  ) { }

  async showLoadingHandler() {
    if (this.loading == null) {
      this.loading = await this.loadingController.create({
        message: 'Aguarde. Estamos processando este arquivo.',
      });
      this.loading.present();
    }
  } 

  public hideLoadingHandler() {
    if (this.loading != null) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  ngOnInit() {
    this.qtdNotifications = localStorage.getItem('qtdNotificationsBadge');
    setInterval(() => { 
       this.qtdNotifications = localStorage.getItem('qtdNotificationsBadge');
    }, 1000);
    
    this.datenow = Date.now();

    this.plt.ready().then(() => {
      let path = this.file.dataDirectory;
      this.file.checkDir(path, MEDIA_FOLDER_NAME).then(
        () => {
          this.loadFiles();
        },
        err => {
          this.file.createDir(path, MEDIA_FOLDER_NAME, false);
        }
      );
    });


    let url = this.global.url + `/api/checkDealer/` + localStorage.getItem('user_id');
    let url2 = this.global.url + `/api/checkClub/` + localStorage.getItem('user_id');

    if (this.platform.is('cordova')) {
      this.nativeHttp.get(url, {}, this.headers)
        .then(data => {
          this.statusDealer = JSON.parse(data['data']);
        }, error => {
          console.log(error);
        });

      this.nativeHttp.get(url2, {}, this.headers)
      .then(data => {
        this.statusClub = JSON.parse(data['data']);
      }, error => {
        console.log(error);
      });
    }
    else {
      this.http.get(url, { headers: this.headers })
      .subscribe(data => {
        this.statusDealer = data;
      }, error => {
        console.log(error);
      });

      this.http.get(url2, { headers: this.headers })
      .subscribe(data => {
        this.statusClub = data;
      }, error => {
        console.log(error);
      });
    }
  }

  loadFiles() {
    this.file.listDir(this.file.dataDirectory, MEDIA_FOLDER_NAME).then(
      res => {
        this.files = res;
      },
      err => console.log('error loading files: ', err)
    );
  }

  async selectMedia() {

    if(this.medias.length == 1)
    {
        alert('Você pode cadastrar apenas uma foto ou video.');
    }
    else
    {
      const actionSheet = await this.actionSheetController.create({
        header: 'Selecione a opção desejada.',
        buttons: [
          {
            text: 'Tirar foto',
            handler: () => {
              this.captureImage();
            }
          },
          {
            text: 'Fazer Vídeo',
            handler: () => {
              this.recordVideo();
            }
          },
          // {
          //   text: 'Record Audio',
          //   handler: () => {
          //     this.recordAudio();
          //   }
          // },
          {
            text: 'Escolher do celular',
            handler: () => {
              this.pickImages();
            }
          },
          {
            text: 'Cancelar',
            role: 'cancel'
          }
        ]
      });
      await actionSheet.present();
    }
  }

  pickImages() {
    this.imagePicker.getPictures({}).then(
      results => {
        for (var i = 0; i < results.length; i++) {
          this.copyFileToLocalDir(results[i]);
        }
      }
    );
  }

  captureImage() {
    this.mediaCapture.captureImage().then(
      (data: MediaFile[]) => {
        if (data.length > 0) {
          this.copyFileToLocalDir(data[0].fullPath);
        }
      },
      (err: CaptureError) => console.error(err)
    );
  }

  recordAudio() {
    this.mediaCapture.captureAudio().then(
      (data: MediaFile[]) => {
        if (data.length > 0) {
          this.copyFileToLocalDir(data[0].fullPath);
        }
      },
      (err: CaptureError) => console.error(err)
    );
  }

  recordVideo() {
    this.mediaCapture.captureVideo({ limit: 1, duration: 20 }).then(
      (data: MediaFile[]) => {
        if (data.length > 0) {
          this.copyFileToLocalDir(data[0].fullPath);
        }
      },
      (err: CaptureError) => console.error(err)
    );
  }

  copyFileToLocalDir(fullPath) {

    this.showLoadingHandler();

    let myPath = fullPath;
    // Make sure we copy from the right location
    if (fullPath.indexOf('file://') < 0) {
      myPath = 'file://' + fullPath;
    }

    const ext = myPath.split('.').pop();
    const d = Date.now();
    const newName = `${d}.${ext}`;

    this.extFile = ext;

    const name = myPath.substr(myPath.lastIndexOf('/') + 1);
    const copyFrom = myPath.substr(0, myPath.lastIndexOf('/') + 1);
    const copyTo = this.file.dataDirectory + MEDIA_FOLDER_NAME;

    this.filedata0 = copyTo;
    this.filedata0name = newName;

    this.medias.push({ name: this.filedata0name, uri: myPath});

    let fileTransfer: FileTransferObject = this.transfer.create();

    let options: FileUploadOptions = {
      fileKey: 'media0',
      fileName: this.filedata0name,
      mimeType: 'multipart/form-data',
      headers: {}
    }
    
    fileTransfer.upload(myPath, this.global.url+'/api/upload-file', options)
    .then((data) => {
      this.hideLoadingHandler();
      // this.loadFiles();
      this.files = this.medias;
      // alert(JSON.stringify(data));
    }, (err) => {
      alert('erro no upload');
      alert(JSON.stringify(err));
      this.hideLoadingHandler();
    })

    // this.file.copyFile(copyFrom, name, copyTo, newName).then(
    //   success => {

    //     alert('OK AQUI');

    //     this.medias.push({ name: this.filedata0name });

    //     let fileTransfer: FileTransferObject = this.transfer.create();

    //     let options: FileUploadOptions = {
    //       fileKey: 'media0',
    //       fileName: this.filedata0name,
    //       mimeType: 'multipart/form-data',
    //       headers: {}
    //     }
        
    //     fileTransfer.upload(myPath, this.global.url + '/api/upload-file', options)
    //       .then((data) => {
    //         this.hideLoadingHandler();
    //         // alert(JSON.stringify(data));
    //       }, (err) => {
    //         // alert('erro no upload');
    //         alert(JSON.stringify(err));
    //       })


    //     this.loadFiles();
    //   },
    //   error => {
    //     alert(JSON.stringify(error));
    //   }
    // );
  }

  openFile(f: FileEntry) {
    if (f.name.indexOf('.wav') > -1) {
      // We need to remove file:/// from the path for the audio plugin to work
      const path = f.nativeURL.replace(/^file:\/\//, '');
      const audioFile: MediaObject = this.media.create(path);
      audioFile.play();
    } else if (f.name.indexOf('.MOV') > -1 || f.name.indexOf('.mp4') > -1) {
      // E.g: Use the Streaming Media plugin to play a video
      this.streamingMedia.playVideo(f.nativeURL);
    } else if (f.name.indexOf('.jpg') > -1) {
      // E.g: Use the Photoviewer to present an Image
      this.photoViewer.show(f.nativeURL, 'Foto');
    }
  }

  deleteFile(f: FileEntry) {
    const path = f.nativeURL.substr(0, f.nativeURL.lastIndexOf('/') + 1);
    this.file.removeFile(path, f.name).then(() => {

      var fileObj = this.medias.find(x => x.name == f.name);

      this.medias.splice(this.medias.indexOf(fileObj), 1)

      this.loadFiles();
    }, err => console.log('error remove: ', err));
  }

  onClickSubmit(f: NgForm, formData) {
    
    if(formData.title == undefined || this.medias.length == 0) 
    {
      alert("Preencha todos os campos!");
    }
    else
    {
      this.showLoadingHandler();
      
      var type = '';
      if(this.extFile == 'jpg')
      {
        type = 'photo';
      }
      else {
        type = 'video';
      }

      let url = this.global.url+"/api/newPublication";


      var files = '';

      for(var i=0; i< this.medias.length; i++)
      {
        files += this.medias[i]['name'];
      }

      let postData = {
        "user_id": localStorage.getItem('user_id'),
        "type": type,
        "title": formData.title,
        "session": formData.session,
        "file": files
      }

      if(this.platform.is('cordova'))
      {
        this.nativeHttp.post(url, postData, this.headers)
            .then(res => {
              alert("Cadastro realizado!");
              this.router.navigate(['home']);
              this.hideLoadingHandler();
            }, error => {
              alert(JSON.stringify(error));
            });
      }
      else {
        this.http.post(url, postData, { headers: this.headers
        }).subscribe(data => {
          alert("Cadastro realizado!");
          this.router.navigate(['home']);
          this.hideLoadingHandler();
          }, error => {
            console.log(error);
        });
      }

      
    }
  }

}
