import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'mundo-troller/:session/:userid',
    loadChildren: () => import('./mundo-troller/mundo-troller.module').then( m => m.MundoTrollerPageModule)
  },
  {
    path: 'experiencias',
    loadChildren: () => import('./experiencias/experiencias.module').then( m => m.ExperienciasPageModule)
  },
  {
    path: 'publicar',
    loadChildren: () => import('./publicar/publicar.module').then( m => m.PublicarPageModule)
  },
  {
    path: 'loja',
    loadChildren: () => import('./loja/loja.module').then( m => m.LojaPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'bio',
    loadChildren: () => import('./bio/bio.module').then( m => m.BioPageModule)
  },
  {
    path: 'publicar-fotovideo',
    loadChildren: () => import('./publicar-fotovideo/publicar-fotovideo.module').then( m => m.PublicarFotovideoPageModule)
  },
  {
    path: 'publicar-audio',
    loadChildren: () => import('./publicar-audio/publicar-audio.module').then( m => m.PublicarAudioPageModule)
  },
  {
    path: 'publicar-artigo',
    loadChildren: () => import('./publicar-artigo/publicar-artigo.module').then( m => m.PublicarArtigoPageModule)
  },
  {
    path: 'publicar-mapa',
    loadChildren: () => import('./publicar-mapa/publicar-mapa.module').then( m => m.PublicarMapaPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'mapas',
    loadChildren: () => import('./mapas/mapas.module').then( m => m.MapasPageModule)
  },
  {
    path: 'termos',
    loadChildren: () => import('./termos/termos.module').then( m => m.TermosPageModule)
  },
  {
    path: 'sobre',
    loadChildren: () => import('./sobre/sobre.module').then( m => m.SobrePageModule)
  },
  {
    path: 'concessionarias',
    loadChildren: () => import('./concessionarias/concessionarias.module').then( m => m.ConcessionariasPageModule)
  },
  {
    path: 'clubes',
    loadChildren: () => import('./clubes/clubes.module').then( m => m.ClubesPageModule)
  },
  {
    path: 'beneficios',
    loadChildren: () => import('./beneficios/beneficios.module').then( m => m.BeneficiosPageModule)
  },
  {
    path: 'clube/:id',
    loadChildren: () => import('./clube/clube.module').then( m => m.ClubePageModule)
  },
  {
    path: 'loja2/:id',
    loadChildren: () => import('./loja2/loja2.module').then( m => m.Loja2PageModule)
  },
  {
    path: 'produtos/:id',
    loadChildren: () => import('./produtos/produtos.module').then( m => m.ProdutosPageModule)
  },
  {
    path: 'concessionarias-lista/:regions',
    loadChildren: () => import('./concessionarias-lista/concessionarias-lista.module').then( m => m.ConcessionariasListaPageModule)
  },
  {
    path: 'calendario',
    loadChildren: () => import('./calendario/calendario.module').then( m => m.CalendarioPageModule)
  },
  {
    path: 'evento/:id',
    loadChildren: () => import('./evento/evento.module').then( m => m.EventoPageModule)
  },
  {
    path: 'evento-sobre/:id',
    loadChildren: () => import('./evento-sobre/evento-sobre.module').then( m => m.EventoSobrePageModule)
  },
  {
    path: 'evento-calendario/:id',
    loadChildren: () => import('./evento-calendario/evento-calendario.module').then( m => m.EventoCalendarioPageModule)
  },
  {
    path: 'novo-veiculo/:type/:id',
    loadChildren: () => import('./novo-veiculo/novo-veiculo.module').then( m => m.NovoVeiculoPageModule)
  },
  {
    path: 'criar-conta',
    loadChildren: () => import('./criar-conta/criar-conta.module').then( m => m.CriarContaPageModule)
  },
  {
    path: 'novo-clube/:type',
    loadChildren: () => import('./novo-clube/novo-clube.module').then( m => m.NovoClubePageModule)
  },
  {
    path: 'nova-concessionaria/:type',
    loadChildren: () => import('./nova-concessionaria/nova-concessionaria.module').then( m => m.NovaConcessionariaPageModule)
  },
  {
    path: 'esqueci-senha',
    loadChildren: () => import('./esqueci-senha/esqueci-senha.module').then( m => m.EsqueciSenhaPageModule)
  },
  {
    path: 'novo-time/:type/:idEvent/:idCategory/:value',
    loadChildren: () => import('./novo-time/novo-time.module').then( m => m.NovoTimePageModule)
  },
  {
    path: 'categorias-evento/:id',
    loadChildren: () => import('./categorias-evento/categorias-evento.module').then( m => m.CategoriasEventoPageModule)
  },
  {
    path: 'pagar/:idEvent/:idCategory/:role1/:role2/:idVehicle/:participants/:value',
    loadChildren: () => import('./pagar/pagar.module').then( m => m.PagarPageModule)
  },
  
















];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
