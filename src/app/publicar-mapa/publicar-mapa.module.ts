import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PublicarMapaPageRoutingModule } from './publicar-mapa-routing.module';

import { PublicarMapaPage } from './publicar-mapa.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PublicarMapaPageRoutingModule
  ],
  declarations: [PublicarMapaPage]
})
export class PublicarMapaPageModule {}
