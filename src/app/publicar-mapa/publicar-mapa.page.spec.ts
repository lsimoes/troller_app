import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PublicarMapaPage } from './publicar-mapa.page';

describe('PublicarMapaPage', () => {
  let component: PublicarMapaPage;
  let fixture: ComponentFixture<PublicarMapaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicarMapaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PublicarMapaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
