import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PublicarMapaPage } from './publicar-mapa.page';

const routes: Routes = [
  {
    path: '',
    component: PublicarMapaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PublicarMapaPageRoutingModule {}
