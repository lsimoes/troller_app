import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../global.service';
import { Platform } from '@ionic/angular';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-novo-veiculo',
  templateUrl: './novo-veiculo.page.html',
  styleUrls: ['./novo-veiculo.page.scss'],
})
export class NovoVeiculoPage implements OnInit {

  type: any;
  loading: any;

  brands: any;
  models: any;
  years: any;
  usages: any;
  colors: any;
  has_insurance = null;
  usagesArr:Array<string> = new Array(); 
  brandId: any;

  name: any;
  chassi: any;
  color: any;
  owner_name: any;
  owner_type: any;
  vehicle_model_id: any;
  engine: any;
  year: any;
  model_year: any;
  plate: any;
  km: any;
  
  vehicle: any;
  idVehicle: any;
  message: any;
  urlEp: any;

  qtdNotifications: any;

  headers = {
    'Content-Type': 'application/json',
    'Authorization': localStorage.getItem('token_type') + ' ' + localStorage.getItem('access_token')
  };

  constructor(
    private global: GlobalService,
    private platform: Platform,
    private http: HttpClient,
    private nativeHttp: HTTP,
    private loadingController: LoadingController,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  async showLoadingHandler() {
    if (this.loading == null) {
      this.loading = await this.loadingController.create({
        message: '',
      });
      this.loading.present();
    }
  } 

  public hideLoadingHandler() {
    if (this.loading != null) {
        this.loading.dismiss();
        this.loading = null;
    }
  }

  ngOnInit() {
    this.qtdNotifications = localStorage.getItem('qtdNotificationsBadge');
    setInterval(() => { 
       this.qtdNotifications = localStorage.getItem('qtdNotificationsBadge');
    }, 1000);

    this.route.paramMap.subscribe(params => {
      this.type = params.get("type");
      this.idVehicle = params.get("id");
    })

    if(this.type == 'edit') {
      this.message = 'Veículo atualizado!';
      this.urlEp = this.global.urlEp + '/api/vehicles/update';
      this.showLoadingHandler();

      let url = this.global.urlEp + '/api/contestants/'+localStorage.getItem('user_id')+'/vehicle/' + this.idVehicle;
      
      if (this.platform.is('cordova')) {
        this.nativeHttp.get(url, {}, this.headers)
          .then(data => {

            this.vehicle = JSON.parse(data['data']);

            this.name = this.vehicle[0].name;
            this.color = this.vehicle[0].color;
            this.owner_name = this.vehicle[0].owner_name;
            this.owner_type = this.vehicle[0].owner_type;
            this.vehicle_model_id = this.vehicle[0].vehicle_model_id;
            this.engine = this.vehicle[0].engine;
            this.year = this.vehicle[0].year;
            this.model_year = this.vehicle[0].model_year;
            this.plate = this.vehicle[0].plate;
            this.km = this.vehicle[0].km;
            // this.usage_profile[] = this.vehicle[0].usage_profile;
            // this.has_insurance = this.vehicle[0].has_insurance;
  
            this.hideLoadingHandler();
  
          }, error => {
            console.log(error);
          });
      }
      else {
        this.http.get(url, { headers: this.headers })
          .subscribe(data => {
  
            this.vehicle = data['data'];

            this.name = this.vehicle.name;
            this.chassi = this.vehicle.chassi;
            this.color = this.vehicle.color;
            this.owner_name = this.vehicle.owner_name;
            this.owner_type = this.vehicle.owner_type;
            this.vehicle_model_id = this.vehicle.vehicle_model_id;
            this.year = this.vehicle.year;
            this.engine = this.vehicle.engine;
            this.model_year = this.vehicle.model_year;
            this.plate = this.vehicle.plate;
            this.km = this.vehicle.km;
            // this.usage_profile[] = this.vehicle.usage_profile;
            // this.has_insurance = this.vehicle.has_insurance;
  
            this.hideLoadingHandler();
  
          }, error => {
            console.log(error);
          });
      }
    }
    else {
      this.message = 'Veículo cadastrado.';
      this.urlEp = this.global.urlEp + '/api/vehicles/create';
    }

    this.getBrands();
    this.getYears();
    this.getUsages();
    this.getColors();
  }

  usageProfile(profile) {
    if(this.usagesArr.indexOf(profile) == -1)
    {
      this.usagesArr.push(profile);
      // alert(this.usagesArr);
    }
    else {
      this.usagesArr.splice(this.usagesArr.indexOf(profile), 1);
    }
  }

  hasInsurance(bool) {
    this.has_insurance = bool;
  }

  //|| formData.usage_profile == undefined || formData.usage_profile == false

  onClickSubmit(formData) {
    // alert(formData.usage_profile);

    if (formData.name == '' || formData.color == '' || formData.owner_type == '' || formData.chassi == '' || formData.vehicle_model_id == '' || formData.year == '' || formData.model_year == '' || formData.plate == '' || formData.km == '' || this.has_insurance == null) 
    {
        this.global.presentAlert('Preencha os campos obrigatórios');
    }
    else 
    {
      this.showLoadingHandler();

      let url = this.urlEp;

      let postData = {
        "id": this.idVehicle,
        "contestant_id": localStorage.getItem('user_id'),
        "name": formData.name,
        "color": formData.color,
        "owner_name": formData.owner_name,
        "owner_type": formData.owner_type,
        "chassi": formData.chassi,
        "vehicle_model_id": formData.vehicle_model_id,
        "engine": formData.engine,
        "year": formData.year,
        "model_year": formData.model_year,
        "plate": formData.plate,
        "km": formData.km,
        "usage_profile[]": this.usagesArr, //this.usagesArr
        "has_insurance": this.has_insurance,
        "delete_images": []
        
      }

      if(this.platform.is('cordova'))
      {
        this.nativeHttp.post(url, postData, this.headers)
            .then(res => {
              
              this.hideLoadingHandler();
              this.global.presentAlert(this.message);
              this.router.navigate(['/bio']);
                
            }, error => {
              alert('Erro ao cadastrar veículo: ' + JSON.stringify(error['error']));
              this.hideLoadingHandler();
            });
      }
      else
      {
        this.http.post(url, postData, { headers: this.headers})
          .subscribe(data => {

            this.hideLoadingHandler();
            this.global.presentAlert(this.message);
            this.router.navigate(['/bio']);
              
            }, error => {
              alert('Erro ao cadastrar veículo: ' + JSON.stringify(error['error']));
              this.hideLoadingHandler();
        });
      }
    }
  }

  getBrands() {

    let url = this.global.urlEp + '/api/vehicles/brands';

    if (this.platform.is('cordova')) {
      this.nativeHttp.get(url, {}, this.headers)
        .then(res => {


          let data = JSON.parse(res['data']);

          this.brands = data['data'];

        }, error => {
          console.log(error);
        });
    }
    else {
      this.http.get(url, { headers: this.headers })
        .subscribe(data => {

          this.brands = data['data'];

        }, error => {
          console.log(error);
        });
    }
  }

  getModels(brandId) {
    let url = this.global.urlEp + '/api/vehicles/models';

    this.brandId = brandId;

    if (this.platform.is('cordova')) {
      this.nativeHttp.get(url, {}, this.headers)
        .then(res => {


          let data = JSON.parse(res['data']);

          this.models = data['data'];

        }, error => {
          console.log(error);
        });
    }
    else {
      this.http.get(url, { headers: this.headers })
        .subscribe(data => {

          this.models = data['data'];

        }, error => {
          console.log(error);
        });
    }
  }

  getYears() {

    let url = this.global.urlEp + '/api/vehicles/years';

    if (this.platform.is('cordova')) {
      this.nativeHttp.get(url, {}, this.headers)
        .then(res => {


          let data = JSON.parse(res['data']);

          this.years = data['data'];

        }, error => {
          console.log(error);
        });
    }
    else {
      this.http.get(url, { headers: this.headers })
        .subscribe(data => {

          this.years = data['data'];

        }, error => {
          console.log(error);
        });
    }
  }

  getUsages() {

    let url = this.global.urlEp + '/api/vehicles/usage-profiles';

    if (this.platform.is('cordova')) {
      this.nativeHttp.get(url, {}, this.headers)
        .then(res => {


          let data = JSON.parse(res['data']);

          this.usages = data['data'];

        }, error => {
          console.log(error);
        });
    }
    else {
      this.http.get(url, { headers: this.headers })
        .subscribe(data => {

          this.usages = data['data'];

        }, error => {
          console.log(error);
        });
    }
  }

  getColors() {

    let url = this.global.urlEp + '/api/vehicles/colors';

    if (this.platform.is('cordova')) {
      this.nativeHttp.get(url, {}, this.headers)
        .then(res => {


          let data = JSON.parse(res['data']);

          this.colors = data['data'];

        }, error => {
          console.log(error);
        });
    }
    else {
      this.http.get(url, { headers: this.headers })
        .subscribe(data => {

          this.colors = data['data'];

        }, error => {
          console.log(error);
        });
    }
  }

}
