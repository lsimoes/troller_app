import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NovoVeiculoPage } from './novo-veiculo.page';

const routes: Routes = [
  {
    path: '',
    component: NovoVeiculoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NovoVeiculoPageRoutingModule {}
