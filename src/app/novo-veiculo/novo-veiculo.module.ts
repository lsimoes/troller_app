import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NovoVeiculoPageRoutingModule } from './novo-veiculo-routing.module';

import { NovoVeiculoPage } from './novo-veiculo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NovoVeiculoPageRoutingModule
  ],
  declarations: [NovoVeiculoPage]
})
export class NovoVeiculoPageModule {}
