import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NovoTimePage } from './novo-time.page';

describe('NovoTimePage', () => {
  let component: NovoTimePage;
  let fixture: ComponentFixture<NovoTimePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NovoTimePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NovoTimePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
