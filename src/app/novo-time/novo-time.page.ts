import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../global.service';
import { Platform } from '@ionic/angular';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-novo-time',
  templateUrl: './novo-time.page.html',
  styleUrls: ['./novo-time.page.scss'],
})
export class NovoTimePage implements OnInit {

  user_id: any;
  vehicles: any;
  idCategory: any;
  idEvent: any;
  value: any;

  loading: any;

  qtdNotifications: any;

  headers = {
    'Content-Type': 'application/json',
    'Authorization': localStorage.getItem('token_type') + ' ' + localStorage.getItem('access_token')
  };

  constructor(
    private global: GlobalService,
    private platform: Platform,
    private http: HttpClient,
    private nativeHttp: HTTP,
    private loadingController: LoadingController,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  async showLoadingHandler() {
    if (this.loading == null) {
      this.loading = await this.loadingController.create({
        message: '',
      });
      this.loading.present();
    }
  } 

  public hideLoadingHandler() {
    if (this.loading != null) {
        this.loading.dismiss();
        this.loading = null;
    }
  }

  ngOnInit() {

    this.qtdNotifications = localStorage.getItem('qtdNotificationsBadge');
    setInterval(() => { 
       this.qtdNotifications = localStorage.getItem('qtdNotificationsBadge');
    }, 1000);
    
    this.user_id = localStorage.getItem('user_id');

    this.route.paramMap.subscribe(params => {
      this.idCategory = params.get("idCategory");
      this.idEvent = params.get("idEvent");
      this.value = params.get("value");
    })

    this.getVehicles();
  }

  onClickSubmit(formData) {
    if (formData.role_responsible_for_vehicle == '' || formData.role == '' || formData.contestant_vehicle_id == '' || formData.max_team_invites == '') 
    {
        this.global.presentAlert('Preencha os campos obrigatórios');
    }
    else if(formData.max_team_invites < 2)
    {
      this.global.presentAlert('Deve ter no mínimo 2 participantes.');
    }
    else 
    {
      this.router.navigate(['/pagar/' + this.idEvent + '/' + this.idCategory + '/' + formData.role_responsible_for_vehicle + '/' + formData.role + '/' + formData.contestant_vehicle_id + '/' + formData.max_team_invites + '/' + this.value]);

      // let postData = {
      //   "contestant_id": this.user_id,
      //   "contest_id": this.idCategory,
      //   "role_responsible_for_vehicle": formData.role_responsible_for_vehicle,
      //   "role": formData.role,
      //   "contestant_vehicle_id": formData.contestant_vehicle_id,
      //   "max_team_invites": formData.max_team_invites,
      // }

      // let url = 'https://jwt.servershot.com.br/troller/portal/api/events/new-team';

      // if(this.platform.is('cordova'))
      // {
      //   this.nativeHttp.post(url, postData, this.headers)
      //       .then(res => {
              
      //         this.hideLoadingHandler();
      //         this.global.presentAlert("Time cadastrado!");
      //         this.router.navigate(['/bio']);
                
      //       }, error => {
      //         alert('Erro ao cadastrar veículo: ' + JSON.stringify(error['error']['data']));
      //         this.hideLoadingHandler();
      //       });
      // }
      // else
      // {
      //   this.http.post(url, postData, { headers: this.headers})
      //     .subscribe(data => {

      //       this.hideLoadingHandler();
      //       this.global.presentAlert("Time cadastrado!");
      //       this.router.navigate(['/bio']);
              
      //       }, error => {
      //         alert('Erro ao cadastrar time: ' + JSON.stringify(error['error']['data']));
      //         this.hideLoadingHandler();
      //   });
      // }
    }
  }

  getVehicles() {

    let url = this.global.urlEp + '/api/contestants/'+this.user_id+'/vehicles';

    if (this.platform.is('cordova')) {
      this.nativeHttp.get(url, {}, this.headers)
        .then(res => {

          let data = JSON.parse(res['data']);

          this.vehicles = data['data'];

        }, error => {
          console.log(error);
        });
    }
    else {
      this.http.get(url, { headers: this.headers })
        .subscribe(data => {

          this.vehicles = data['data'];

        }, error => {
          console.log(error);
        });
    }
  }

}
