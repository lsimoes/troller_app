import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NovoTimePageRoutingModule } from './novo-time-routing.module';

import { NovoTimePage } from './novo-time.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NovoTimePageRoutingModule
  ],
  declarations: [NovoTimePage]
})
export class NovoTimePageModule {}
