import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NovoTimePage } from './novo-time.page';

const routes: Routes = [
  {
    path: '',
    component: NovoTimePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NovoTimePageRoutingModule {}
