import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ClubesPage } from './clubes.page';

describe('ClubesPage', () => {
  let component: ClubesPage;
  let fixture: ComponentFixture<ClubesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClubesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ClubesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
