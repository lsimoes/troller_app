import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../global.service';
import { Platform } from '@ionic/angular';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-clubes',
  templateUrl: './clubes.page.html',
  styleUrls: ['./clubes.page.scss'],
})
export class ClubesPage implements OnInit {
  status: any;
  clubs: any;
  loading: boolean= false;
  qtdNotifications: any;

  constructor(
    private global: GlobalService,
    private platform: Platform,
    private http: HttpClient,
    private nativeHttp: HTTP,
    private loadingController: LoadingController,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {

    this.qtdNotifications = localStorage.getItem('qtdNotificationsBadge');
    setInterval(() => { 
       this.qtdNotifications = localStorage.getItem('qtdNotificationsBadge');
    }, 1000);

    this.status = 2;
    
    let headers = {
      'Content-Type': 'application/json',
      'Authorization': localStorage.getItem('token_type') + ' ' + localStorage.getItem('api_access_token')
    };

    let url = this.global.url + `/api/getClubs`;
    let url2 = this.global.url + `/api/checkClub/` + localStorage.getItem('user_id');

    if (this.platform.is('cordova')) {
      this.nativeHttp.get(url, {}, headers)
        .then(data => {

          this.clubs = JSON.parse(data['data']);
          this.loading = !this.loading;
        }, error => {
          console.log(error);
        });

      this.nativeHttp.get(url2, {}, headers)
        .then(data => {
          this.status = JSON.parse(data['data']);
        }, error => {
          console.log(error);
        });
    }
    else {
      this.http.get(url, { headers: headers })
        .subscribe(data => {
          this.clubs = data;
          this.loading = !this.loading;
        }, error => {
          console.log(error);
        });

      this.http.get(url2, { headers: headers })
        .subscribe(data => {
          this.status = data;
        }, error => {
          console.log(error);
        });
    }
  }

}
